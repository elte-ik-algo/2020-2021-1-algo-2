# Stringek - Mintaillesztés (keresés)

* Kovácsné Pusztai Kinga által összeállított anyag ebben a mappában a mintaillesztes.pdf fájl
* Kommentjeim, lásd alább

## Bevezetés és Naiv/BF algoritmus

* Magyarázó kommentjeim: https://youtu.be/QO8sWN8x13Q
* Jegyzetek, amiket vetítek: ebben a mappában a 01_AltalanosEsNaiv_MagyarazatJegyzet.txt fájl

## Knuth--Morris--Pratt-algoritmus

* Magyarázó kommentjeim: https://youtu.be/YzAN9smPteI
* Jegyzetek, amiket vetítek: ebben a mappában a 02_KMP_MagyarazatJegyzet.txt fájl

## Quick Search

* Magyarázó kommentjeim: https://youtu.be/XclHp9UjJaU
* Jegyzetek, amiket vetítek: ebben a mappában a 03_QS_MagyarazatJegyzet.txt fájl
