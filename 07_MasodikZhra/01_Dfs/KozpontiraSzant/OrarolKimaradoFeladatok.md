# Az órán le nem adott feladatok a pdf végéről

## Nagyobb feladat 1.

* Ábrázoljuk irányított gráfként az alábbi folyamat lépéseit a köztük levő előfeltételviszonyokkal. Járjuk be mélységien, s erre építve határozzunk meg egy topologikus sorrendet
    * A félév elején jelentkezni kell egy kurzusra. A félév során a jegyszerzéshez két zh-t kell megírni (előbb az elsőt, majd a másodikat), és be kell adni két házi feladatot (mindegy mikor, csak a szorgalmi időszak alatt legyen). Ezek után vagy gyakuv-re megy a hallgató, vagy vizsgázni. A gyakuv után is elérhető a vizsga.

### Megoldás

* Az alábbi ASCII-art próbál a gráf lenni. Értelemszerű csúcsnevekkel, talán a "SZORGIDVÉG" ér meg egy kis magyarázatot. Azt akarja kifejezni, hogy ha minden szorgalmi időszakbeli esemény megvan, akkor is még meg kell várni az utolsó hetet, miután vizsgázhatunk (egy ideális gázban legalábbis)

```
 |-------> HF1 -----------\         /----> GYAKUV ---\
 |                         \       /                  \
 |                         \/     /                   \/
JEL ---> ZH1 ---> ZH2 ---> SZORGIDVÉG--------------> VIZSGA
 |                         /\
 |                         /
 |-------> HF2------------/
```

* Mélységi bejárás:
```
 |-------> HF1 -----------\         /----> GYAKUV ---\
 |         2/9             \       /         4/7      \
 |                         \/     /                   \/
JEL ---> ZH1 ---> ZH2 ---> SZORGIDVÉG--------------> VIZSGA
1/16    12/15    13/14     /\  3/8                     5/6
 |                         /
 |-------> HF2------------/
          10/11
```

* Ez alapján egy lehetséges topologikus rendezés:
    * <JEL, ZH1, ZH2, HF2, HF1, SZORGIDVÉG, GYAKUV, VIZSGA>

## Nagyobb feladat 2.

* Írjuk meg a mélységi bejárásra alapozott topologikus sorrendet megállapító algoritmust éllistás reprezentációra

### Megoldás

* Éllistát adunk át, a csúcsok kulcstípusa legyen T
* T és [0..n-1] között legyen automatikus konverzió (avagy az egyszerűség kedvéért jelölhessük így a tömb indexeit)
* Egy sorral térünk vissza, benne a csúcsok topologikus sorrendje
* Mélységi bejárást csinálunk, de semmire nincs szükségünk, csak a csúcsok 3 állapotára (fehér, szürke, fekete)
* Egy verembe dobáljuk befejezéskor a csúcsokat
* A végén ezt rakjuk át a sorba
* Ha visszaélt találunk, az üres sort adjuk vissza (jó, ez nem teljesen korrekt, mert a 0 csúcsú gráfra is ezt adná vissza, de ott végülis az valid sorrend - a valóságban ez nullpointer vagy leginkább exception lenne)
* A visit függvény megkapja a vermet, hogy tudjon belerakni, illetve ez igazzal tér majd vissza, ha az adott részfában nem volt kör, és hamissal ha volt

```
ts(adj : E1<T>*[n]) : Queue<T>
  FOR i := 0 to n-1
    c[i] := W
  s := Stack<T>()
  q := Queue<T>()
  FOR i := 0 to n-1
    HA c[i] = W és !visit(adj, c, i, s)
      return q
   AMÍG !s.isEmpty()
     q.add(s.pop())
   return q
```

* Az i. éllistán megyünk végig
* Ha fehér csúcsot látunk, megyünk tovább - ha innen kiindulva kört találtunk, akkor kör van a komponensben, rögtön ki is lépünk
* Ha szürkét találunk, akkor ez maga a kör, tehát megint csak return false (itt az olvashatóság, és az eredeti algoritmushoz való hasonlóság miatt nem vontam össze az ágakat, de persze szabadna)
* A végén berakjuk a csúcsot a verembe és igazzal térünk vissza, ha nem volt kör

```
visit(adj : E1<T>*[n], c : {W,G,B}[n], i : T, s : Stack<T>) : L
  c[i] := G
  p := adj[i]
  AMÍG p != NULL
    HA c[p->key] = W
      HA !visit(adj, p->key, time, s)
        return false
    KÜLÖNBEN HA c[p->key] = G
      return false
  c[i] := B
  s.push(i)
  return true
```

## Nagyobb feladat 3.

* Döntsük el az alábbi gráfról, hogy félig összefüggő-e
* Házi feladat
