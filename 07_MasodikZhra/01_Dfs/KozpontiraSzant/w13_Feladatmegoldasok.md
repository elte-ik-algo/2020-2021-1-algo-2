# W13 - 2020. 12. 09.

* Áttekintettük az algoritmust, ide leírom a pszeudokódot, a videóban magyarázat is hangzik el

```
dfs(g : Graph)
  FOR EACH u eleme g.V
    u.c := W
    u.pi := NULL
    u.d := 0
    u.f := 0
  time := 0
  FOR EACH u eleme g.V
    HA u.c = W
      visit(u, time)
```

```
visit(u : Vertex, time& : N)
  u.c := G
  time := time+1
  u.d := time
  process(u)
  FOR EACH v eleme neighbours(u)
    HA v.c = W
      v.pi := u
      visit(v, time)
  time := time+1
  u.f := time
  u.c := B
```

## Feladatok:

### 1. blokk

* A dfs() algoritmus kék és vörös részében is egy-egy azonos gyűjteményt bejáró ciklust látunk egymással szekvenciában. Nem lehetne-e ezeket összevonni?
    * A time inicializálás akár kimehet az első ciklus elé, hiszen ott nem használjuk
    * Viszont ha az első csúcsra inicializálása után rögtön meghívjuk a visit()-et, minden olyan csúcs, amibe onnan megy él még inicializálatlan attribútumokkal várja, hogy felderítsék, definiálatlan értékekre pedig nem írhatunk elágazásbeli feltételt
    * Tehát nem
* Irányítatlan esetben tudjuk, ha a szomszédja b, akkor b szomszédja is a. Ekkor hogy lehet, hogy mégis minden csúcsot csak egyszer jár be?
    * Amikor bejárjuk a-t, akkor szürkére állítjuk mielőtt a gyerekeit vizsgálnánk
    * Amikor b-hez érünk, azt is szürkére állítjuk. Innen természetesen megnézzük a-t, de már szürke lesz, tehát nem fogjuk újra bejárni
    * Körfigyeléses bejárásnál vigyázni kell, hogy ilyenkor ne tekintsük visszaélnek ezt az élt (persze ha van párhuzamos, vagy ez egy hurokél, akkor az valóban visszaél)
* Lehet-e olyan adattag, aminek az értéke végig az marad, amit inicializáláskor megkap?
    * A time végig nő, a d és f ezt követi, a 0 egy nem valid végső érték nekik. A c-re ez ugyanígy igaz, a végén mind fekete lesz
    * A π viszont az elsőként feldolgozott csúcsnál NULL marad. Sőt, az összes olyan csúcsnál így marad, ami a mélységi fák gyökereit adják meg - ezek azok, amiknek visitjét közvetlenül a dfs() algoritmus ciklusából hívunk meg
    * Üres gráf esetén a time is 0 marad, a csúcs szerinti attribútumok pedig ekkor nem léteznek
* Miért adjuk át time-ot referencia szerint?
    * Mert átírjuk értékét
    * Másnak nem, de az amúgy is "objektum", ezért nem is kéne a referenciajelet kitenni

### 2. blokk

* Ha egy v csúcsra az algoritmus végére v.f = v.d + 4, ez szemléletesen mit jelent?
    * Világos, hogy d mindig kisebb, mint f - egy csúcsba előbb lépünk be, mint belőle ki
    * v-ből felfedezhetünk több csúcsot is, akár láncban, akár nem, teljesen mindegy, garantáltan előbb fejezzük be ezeket, mint v-t
    * Azaz, ha v-ből 2 csúcsot fedezünk fel, az 4-gyel dobja meg a time-ot (d és f mindkettőnek)
    * Viszont v f-je eleve eggyel több, mint a d-je
    * Tehát v.f és v.d különbségének páratlannak kell lennie
    * Tehát azt jelenti, hogy hiba történt
* Egy adott pillanatban hány szürke csúcs van?
    * A mélységi bejárás során mindig egy ágon megyünk egész addig, amíg csak találunk fehér csúcsot
    * Egy ilyen ág csúcsai (a részfa gyökerétől kezdve) a szürkék, tehát annyi szürke van, amilyen mélyen vagyunk a fában
* Képzeljünk el egy gráfot, ami egy labirintust ír le (az élek az utak, a csúcsok a kereszteződések/sarkok, az egyik csúcs meg van címkézve az EXIT felirattal, ide kéne eljutni). Valahol a labirintus közepén állsz és nem látsz rá a teljes labirintusra mindig csak az aktuális csúcsra (szóval mintha ott lennél a valóságban). A szélességi vagy a mélységi bejárás stratégiájával jutsz ki várhatóan könnyebben/hamarabb a labirintusból? Miért? Egyáltalán  biztos-e hogy kijutsz ezen stratégiákkal (feltéve, hogy van EXIT és van a startból út is oda)?
    * A mélységi bejárás épp a labirintusból kijutás logikájával, egy fal mellett a lehető legtovább menve, majd szükség esetén visszafordulva halad
    * A szélességi bejárás viszont csapong a kiindulóponttól közel azonos távolságra levő, de egymástól távoli csúcsok között

### Kahoot1

* Az első kérdés hibás volt

### Algoritmusírás

* Írd át az algoritmust úgy, hogy a c és π adattagok frissítését és használatát kihagyod belőle
* Írd át az algoritmust úgy, hogy a mélységi és befejezési számokat külön szekvencia szerint számolja, tehát annak a csúcsnak, amibe először lépünk be, a mélységi száma 1, amibe k-adiknak, annak k, amelyikbe utolsónak, annak pedig n legyen; és ugyanez legyen igaz a befejezési számra is. Mindkettő legyen egyedi és [1..n]-beli. Ezzel vajon vesztünk információt a hivatalos változathoz képest?

* Mindkettő egyben

```
dfs(g : Graph)
  FOR EACH u eleme g.V
    u.d := 0
    u.f := 0
  dtime := 0
  ftime := 0
  FOR EACH u eleme g.V
    HA u.d = 0
      visit(u, dtime, ftime)
```

```
visit(u : Vertex, dtime& : N, ftime& : N)
  dtime := dtime+1
  u.d := dtime
  process(u)
  FOR EACH v eleme neighbours(u)
    HA v.d = 0
      visit(v, dtime, ftime)
  ftime := ftime+1
  u.f := ftime
```

* Információvesztés: azt most is tudjuk, hogy melyik csúcsot fedeztük fel korábban, és melyiket később. Ezt a befejezésekre is tudjuk. De azt nem tudjuk, hogy hány csúccsal végeztünk egy csúcs kezdése és befejezése között, azaz hogy hány jött belőle. Végső soron ezt is meg tudjuk állapítani a többi csúcs számaiból, de pusztán egy csúcsot vizsgálva nem

### Algoritmusírás és lejátszós feladat

* Írjuk meg az algoritmust tömbös reprezentációra

* TFH: c, pi, d, f globális változók

```
dfs(g : {0,1}[n,n])
  FOR i := 0 to n-1
    c[i] := W
    pi[i] := NULL
    d[i] := 0
    f[i] := 0
  time := 0
  FOR i := 0 to n-1
    HA c[i] = W
      visit(g, i, time)
```

```
visit(g : {0,1}[n,n], i : [0..n-1], time& : N)
  c[i] := G
  time := time+1
  d[i] := time
  process(g, i)
  FOR j := 0 to n-1
    HA g[i,j] = 1 és c[j] = W
      pi[j] := i
      visit(g, j, time)
  time := time+1
  f[i] := time
  c[i] := B
```

* Játsszuk le a jegyzet 7. oldalán levő példát

![Lejátszós feladat DFS](w13_DfsPelda.png)

### Kahoot2

### Élek osztályzása - első blokk

* Azt tudjuk, ha nem ábécésorrendben vesszük a csúcsokat, mások lehetnek a fák gyökerei.
De lehetnek mások a feszítőerdő fáiban résztvevő csúcsok is? Lehet-e olyan, hogy egyik bejárással feszítőfát kapunk, a másik sorrend szerinti bejárással több fából álló erdőt ugyanarra a gráfra?
    * Lehet, irányított esetben, ha egy olyan csúccsal kezdünk, amibe vezet él
* Azt tudjuk, hogy a színezés elhagyható, mert ami információt tartalmaz, azt a d/f számokból ki tudjuk következtetni. De vajon megírható-e úgy az algoritmus – körfigyeléssel! –, hogy a d/f számokat hagyjuk el, és a színezést tartjuk meg?
    * A körfigyeléshez a visszaéleket kell keresnünk. Visszaél akkor van, amikor a saját ősünkbe jutunk vissza (szürke csúcsba). A keresztél/előreél közötti döntéshez kell más is a csúcs színén kívül (ami a fekete), de itt nem. A szürke szín pedig ellenőrizhető a számokból: ha d már nem 0, de f még 0, akkor szürke a célcsúcs
* Igaz-e: a faél egy fa éle?
    * Igaz, a mélységi feszítőerdő egyik fájának egyik éle
