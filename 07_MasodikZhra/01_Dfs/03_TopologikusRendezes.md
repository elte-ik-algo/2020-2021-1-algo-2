# Gráfok

## Topologikus rendezés

* DAG-okra értelmezett fogalom
* Egy gráf topologikus rendezése a csúcsok egy olyan nem feltétlen egyértelmű felsorolása, amire minden a gráfbeli (u,v) élre teljesül, hogy a sorrendben u megelőzi v-t
    * Azaz ha egy ilyen sorbarendezésben u megelőzi v-t, akkor a gráfban nem lehet v-ből u-ba menő él (se út - hiszen DAG, azaz körmentes)
    * Ha a gráf csúcsait egy munka részeinek, a köztük levő éleket a munkarészek közötti előfeltétel-viszonyoknak vesszük, akkor a topologikus rendezés a munka elvégzéséhez szükséges lépések egy lehetséges sorrendét adja meg
    * Ha A-ból és B-ből is vezet él C-be, akkor mondhatjuk, hogy A és B is előfeltétele C-nek, azaz C csak akkor végezhető el, ha A és B is kész. Ekkor topologikus sorrend az A, B, C és a B, A, C is, hiszen A és B egymáshoz képest levő sorrendje nem definiált

### Meghatározása

* Futtassuk le a mélységi bejárást a gráfon, közben figyeljük a visszaéleket, hogy eldönthessük, egyáltalán DAG-e - azaz van-e topologikus sorrendje
* A DFS közben jegyezzük fel minden csúcshoz a befejezési számokat
* A topologikus rendezést megkapjuk, ha a befejezési számok szerinti csökkenő sorrendben felsoroljuk a csúcsokat
* Gyakorlati tipp: befejezéskor rakjuk a csúcsot egy verembe, a végén ezen verem elemeit írassuk ki

#### Példa

* TODO

### Alternatív módszer

* "Algoritmus":
    * 1. Határozzuk meg a csúcsok befokait
    * 2. Egy 0 befokút írjunk ki (ilyen mindig lesz, ha DAG - az algoritmus későbbi lépéseiben is)
    * 3. Töröljük a csúcsot, éleivel együtt - ami módosítja a többi befokot
    * 4. GOTO 2.
* Implementációs tipp
    * Nem szerencsés, ha effektíve törlünk az eredeti gráfból
    * Az elején nézzük végig a csúcsokat, mentsük el a befokokat s rakjuk egy sorba (Queue) azokat a csúcsokat, ahol 0 a befok
    * Menjünk végig ezeken a csúcsokon, és az aktuális csúcs kimenő éleinek célcsúcsaira csökkentsük az elmentett befokokat
        * Ahol 0 lett, azt rakjuk be a sorba
    * Körfigyelés könnyedén hozzáadható plusz műveletigény nélkül (számláljuk, minden csúcsot kivettünk-e a sorból)