# Gráfok

## Félig összefüggőség irányított gráfban

* Egy irányított gráf félig összefüggő, ha igaz rá, hogy minden u, v csúcsra van vagy u-ból v-be, vagy v-ből u-ba menő irányított út
    * Ez "megengedő vagy", azaz az valid, ha u és v között oda-vissza is van út, de mindenesetre ez nem elvárás
    * Ebből következik, hogy az erősen összefüggő gráf (azaz, aminek egy erősen összefüggő komponense van) egyben félig összefüggő is
* Lépései:
    * 1. Határozzuk meg az erősen összefüggő komponenseit
    * 2. Készítsük el a komponensgráfot
        * Az erősen összefüggő komponenseket "vonjuk össze" egy csúcsba, a köztük levő éleket hagyjuk el, a belőlük kimenő/bemenő éleket tartsuk meg egyszeres számossággal
        * Azaz ha A és B egy komponens, valamint C egy másik, és A-ból meg B-be és C-be, B-ből A-ba és C-be, akkor AB és C lesz a két csúcs, és AB-ből egy él megy majd C-be. Ha C-ből is menne A-ba vagy B-be, akkor eleve egy erősen összefüggő komponensben lettek volna
    * 3. Ha most, van két olyan komponens, ami között nincs út, biztosan nem lesz félig összefüggő a gráf
    * 4. Tudjuk, hogy a komponensgráf DAG - ha nem így lenne, akkor lenne irányított kör, de akkor az azt jelenti, hogy a körben részt vevő eredeti csúcsok között volt kör..., azaz azok egy erősen összefüggő komponenst alkotnak. De akkor egy csúcs lennének a komponensgráfban. Mivel ez DAG, van értelme rá topologikus sorrendet venni. Nézzünk egy ilyet
    * 5. Akkor félig összefüggő a gráf, ha u és v a topologikus sorrendben két tetszőleges egymást közvetlenül követő csúcs esetén van (u,v) él a komponensgráfban
        * Megj.: ha ez igaz, akkor egyébként biztosan csak egy féle topologikus sorrend lesz!
* A műveletigény így Θ(n+e) tud lenni. Floyd-Warshallal is megoldható, de annak Θ(n<sup>3</sup> a műveletigénye)

### Példa

* Az algoritmus első lépése az erősen összefüggő komponensek meghatározása - ehhez vegyük az előző fejezetbeli példát. Álljon itt emlékeztetőül a megoldása:

![Erősen összefüggő komponensek](04_Megoldas.png)

* TODO