# Minimális költségű feszítőfák

* Angolul Minimum Spanning Tree (MST)
* Alapfeladat: Adott egy összefüggő, irányítatlan, élsúlyozott gráf. Keressük olyan részgráfját, aminek a csúcsai azonosak az eredeti gráf csúcsaival, a részgráf fa (e kettő feltétel jelenti azt, hogy "feszítőfa"), és éleinek összesített súlya minimális
    * Miért összefüggő? - mert nem lehetne feszítőfája, ha nem lenne az
    * Miért irányítatlan? - mert általában ezen feladatoknak (közmű-vezetékek, autópálya-építés, stb.) így van értelmük; egyébként irányított gráfokra a lenti módszerek nem is működnének!
    * Miért élsúlyozott? - megint csak: általában így értelmesek a való életből vett feladatok, viszont az ismertetett algoritmusok élsúlyozatlan esetre is működőképesek, viszont ilyenkor a kisebb komplexitású szélességi bejárás is előállítaná a megoldást

## Magyarázat az előadásjegyzethez

* A jegyzetet lásd előadáson illetve az előadó honlapján, ezért én itt most csak arra törekszem, hogy a szimbólumokat és a definíciókat érthetővé tegyem, elmagyarázzam:
* Az alapalgoritmus a következőképpen szól:
    * Vegyünk egy üres élhalmazt
    * n-1 alkalommal vegyünk ehhez hozzá mindig egy "biztonságosan hozzávehető" élt
    * A végén a részgráf csúcsai az eredeti gráf csúcsai, az élei az így felépített élhalmaz lesznek
* Jelölések:
    * Az épülő élhalmazt majd A-nak hívjuk
    * A kész feszítőfát T-vel jelöljük, ami egy (V,T<sub>E</sub>) pár, azaz a csúcsai az eredeti gráf csúcsai az éleire pedig igaz, hogy T<sub>E</sub> részhalmaza E-nek (nem feltétlen valódi részhalmaz, G lehet, hogy eredetileg is fa volt)
    * A végén A = T<sub>E</sub> lesz
    * Az MST a minimal spanning tree kifejezés rövidítése, ez a tulajdonság lesz igaz T-re (ez nem feltétlen egyedi)
* Két kérdés adódik: miért n-1 kör van és mi az a "biztonságosan hozzávehető"?
    * n-1 élet veszünk fel, mert n csúcs van, egy n csúcsú fának biztosan n-1 éle van
    * Biztonságosan hozzávehető egy él, ha egy A-hoz megfelelően megválasztott vágásban könnyű él
    * Ez persze újabb kérdéseket vet fel... :) Mi az a vágás? Hogy válasszam meg A-tól függően? Mi az, hogy egy él egy vágásban könnyű?
* A "vágás" a gráf csúcsainak két nemüres részhalmazra való bontása, osztályozása. Minden csúcs pontosan az egyikbe kerül
* A lehetséges vágások közül nekünk egy olyat kell választani, ami elkerüli A-t
    * Akkor kerüli el a vágás A-t, ha A minden éle (A ugye egy élhalmaz) olyan, hogy a két végpontja ugyanabban a halmazban van (azaz a vágás két részhalmaza között nem lehet A-beli él)
    * Ezt úgy mondjuk szépen, hogy A egyik éle se keresztezi a vágást (a keresztezés jelentené azt, hogy a két halmaz között megy az él)
* Ha van egy vágás, ami elkerüli A-t, akkor a vágás egyik legkisebb súlyú élét nevezzük könnyűnek
    * Az, hogy a vágás éle, az pedig azt jelenti, hogy a vágásban szereplő két csúcshalmaz között megy, azaz a már bevezetett fogalommal élve keresztezi azt
* Foglaljuk össze:
    * Tegyük n-1-szer a következőt:
    * Vágjuk fel két részre a csúcsokat úgy, hogy az eddigi feszítőfa-élek ne menjenek a két halmaz között, majd a két halmaz között futó élek közül válasszuk ki az egyik legolcsóbbat és vegyük a feszítőfához
    * Ekkor nyilván mindig a legolcsóbb olyan élt választjuk, ami egy leválasztott részt az épülő feszítőfába épített, így hát minden csúcsot végül is a legolcsóbb éllel értünk el

## Példa

* Tekintsük az alábbi gráfot:

![MST példa](01_MstPelda.png)

* Lévén 6 csúcsa van, 5 körben fogjuk elkészíteni a fát
* A vágásokat abszolút "önkényesen" választom, így hát a sorrend nincs kőbe vésve

* 1.
    * A = {}
    * S = {A,B,C}, következésképp V\S = {D,E,F}
    * A vágást keresztező élek: (B,D), (B,E), (C,E)
    * Ebből a minimális: (B,E)
* 2.
    * A = {(B,E)}
    * S nem lehet ugyanaz, mert a B,E él már keresztezne A-ra
    * S = {A, C}, V\S = {B, D, E, F}
    * A szóba jöhető élek: (A,B), (B,C), (C,E)
    * Minimális: (A,B)
* 3.
    * A = {(A,B), (B,E)}
    * S = {A,B,E}, V\S = {C, D, F}
    * Élek: (A,C), (B,C), (B,D), (C,E), (D,E), (E,F)
    * Minimális: (D,E)
* 4.
    * A = {(A,B), (B,E), (D,E)}
    * S = {A,B,C,D,E}, V\S = {F}
    * Élek: (D,F), (E,F)
    * Minimális: (E,F)
* 5.
    * A = {(A,B), (B,E), (D,E), (E,F)}
    * S = {A,B,D,E,F}, V\S = {C}
    * Élek: (A,C), (B,C), (C,E)
    * Minimális: (B,C)
* A feszítőfa élei: {(A,B), (B,C), (B,E), (D,E), (E,F)}
* A példában azt láthattuk, az A halmaz folyamatosan szomszédos csúcsokkal "terjeszkedik". Ez a fajta működés nem törvényszerű, elvileg szigetszerűen is létrejöhet a végeredmény (a Prim-algoritmus ilyen terjeszkedős lesz, a Kruskal pedig inkább szigetes)

## Piros-kék algoritmus [kiegészítő anyag]

* Az alábbiakban ismertetett algoritmus csupán egy másik megfogalmazás, szemléltetés a minimális feszítőfa keresési problémára
* Gyakorlaton erről nem volt szó, nem is kell visszaadni se zh-n, se vizsgán, a célom ezzel csak az, hogy mégjobban megértsétek a vizsgán esetleg előforduló definíciókat!
* Vezessük be az élekre a "piros" és "kék" színezést. Kékek lesznek azok az élek, amik biztosan a feszítőfa részei és pirosak, amelyek biztosan nem
* Kezdetben minden él fehér. Az algoritmus e db körből áll, ennek során mindig egy élt színezünk be. Ami egyszer be lett színezve, már olyan is marad
* A piros élek meghatározásához használjuk a piros szabályt:
    * Vegyünk egy kört, ami nem tartalmaz piros élt (kéket tartalmazhat), ennek a legnagyobb költségű élét fessük pirosra
        * Logikus, hiszen a kör minden éle biztos nem lesz a feszítőfa része, és akkor már a legdrágábbat hagyjuk ki (lehet, hogy többet is ki fogunk, ha az másik kör része), a feladat végső soron a körök eliminálása
    * A kék élekhez a kék szabályt használjuk:
        * Vegyük a csúcsoknak egy X nem üres részhalmazát, úgy, hogy az X-ből ne vezessen ki kék él (nem kell, hogy X összefüggő legyen, nem kell, hogy benne kék élek fussanak), a legkisebb kivezető élet fessük kékre
            * Érthető, hiszen ha egy csúcshalmaz még nem volt rácsatlakoztatva a rendszerre, érdemes a legolcsóbb él mentén ezt megtenni
* A szabályokat lehet bármilyen sorrendben alkalmazni
* Világos, hogy az előző részben tárgyalt alapalgoritmus gyakorlatilag a kék szabályok ismétlését jelenti
    * Az A élhalmaz a kék élek egyre gyarapodó halmaza
    * A vágás az X és a V\X mentén jön létre, hiszen nem vezethet X és V\X között kék (A-beli) él, azaz a vágásnak el kell kerülnie a kék éleket
    * Most az X-en kívüli legolcsóbb élt adjuk hozzá a kék élekhez, azaz A-hoz hozzáadjuk az X és V\X közötti legolcsóbb élt: azaz azt a legolcsóbb élt, ami keresztezi a vágást, azaz a könnyű élt!
* Nézzünk egy lejátszást a fenti példára
    * 1. Piros szabály
        * {A,B,C} egy kör, nincs piros éle, legyen hát a legdrágább, (A,C) piros!
    * 2. Kék szabály
        * {F,B,C} egy tetszőleges nem üres halmaz, amiből nem vezet ki kék él, a legolcsóbb kivezető él, a (B,E) legyen kék
    * 3. Piros szabály
        * {D,E,F} egy kör, legyen (D,F) piros
    * 4. Kék szabály
        * {B,E,A}, legyen (D,E) kék
    * 5. Piros szabály
        * {B, C, E}, (C,E) piros
    * 6. Kék szabály
        * {A,C}, pl. (A,B) lehet kék
    * 7. Piros szabály
        * {B,D,E}, legyen (B,D) piros
    * 8. Kék szabály
        * {C,F}, legyen (B,C) kék
    * 9. Kék szabály (nincs már kör)
        * {F}, (E,F) legyen kék
* Tehát a kék élek: {(B,E), (D,E), (A,B), (B,C), (E,F)}, pontosan mint az előző megoldással

## Implementációk

* Ezt a vágásos, biztonságos éles szisztémát kis gráfra könnyen tudtuk szemléltetni, de mégis hogy algoritmizáljuk? Mikor mi legyen a használt vágás? Erre veszünk két implementációt
