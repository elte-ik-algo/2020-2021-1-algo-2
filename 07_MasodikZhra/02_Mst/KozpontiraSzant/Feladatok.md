# MST témakör feladatai, órán ezekre nem jutott idő

## MST általános

### Miért nehéz így implementálni ezeket az algoritmusokat?

* Mert sok elágazás van bennük, sok olyan kérdés, ami ránézésre persze jól látszik, de egy algoritmusnak, ami egyszerre egy csúcsot vagy egy élt lát, nehezebb, főleg optimálisan eldönteni, melyiket vegye sorra

### Végezd el a fenti algoritmusokat ezen a gráfon:

```
          0          3
    a----------b----------c
    \         /|\         /
     \       / | \       /
      \    1/  |  \4    /
      6\   /   |   \   /3
        \ /    |    \ /
         d   -1|     e
          \    |    /
           \   |   /
           9\  |  /5
             \ | /
              \|/
               f
```

* Általános algoritmus - például
    * Első kör
        * A = {}
        * S = {a}
        * Tehát az a-ból kimenő minimális élt vesszük hozzá: (a,b)
    * Második kör
        * A = {(a,b)}
        * S = {a,b,d,e}
        * Ilyenkor persze S komplementerében is gondolkodhatunk. Amúgy tényleg bármi lehet, az előző halmazt bővíthettem is volna b-vel, a lényeg, az eddig felvett élek mind benne legyenek, vagy mind ne legyenek benne
        * (b,f) a minimális
    * Harmadik kör
        * A = {(a,b), (b,f)}
        * S = {c,d,e}
        * (b,d) a minimális, azaz a "könnyű", ezt vesszük fel most
    * Negyedik kör
        * A = {(a,b), (b,d), (b,f)}
        * S = {a,b,c,d,f}
        * (c,e) a könnyű él
    * Ötödik kör
        * A = {(a,b), (b,d), (b,f), (c,e)}
        * Már minden csúcshoz van él, de egyelőre az {a,b,d,f} és a {c,e} külön van, azaz...
        * S = {c,e} lehetséges
        * (b,c) a legolcsóbb
    * Végső A = {(a,b), (b,c), (b,d), (b,f), (c,e)}

    ```
          0          3
    a----------b----------c
              /|          /
             / |         /
           1/  |        /
           /   |       /3
          /    |      /
         d   -1|     e
               |
               |
               |
               |
               |
               f
    ```

* Piros-kék
    * Első kör
        * {a,b,c} egy nemüres csúcshalmaz, amiből nem vezet ki kék él, tehát a legolcsóbb legyen kék
        * (b,f) kék
    * Második kör
        * {c} önmagában is ilyen
        * (c,e) mondjuk legyen kék (nem determinisztikus, eleve a szabályok és a halamzok megválasztása se, de itt is volt két minimális él)
    * Harmadik kör
        * {a,b,d} egy kör, a legdrágább éle biztosan nem kell
        * (a,d) piros
    * Negyedik kör
        * Válasszunk megint piros szabályt, ez teljesen önkényes. Persze kék szabályok választásával elég hamar lerendezhető az egész, és teljesen szabályos úgy is, én most minden élt be fogok színezni a példa kedvéért
        * {a,b,d,f} is egy kör, de van piros éle, ezért ez nem választható
        * De {b,d,f} igen, és a legdrágább élét be is színezzük
        * (d,f) piros
    * Ötödik kör
        * Most legyen egy kék szabály
        * {b,d,f} akár választható lenne, mert attól még hogy kör, egyben csúcshalmaz is, de mivel b-ből vezet ki kék él, ezért ezt nem szabad
        * Válasszuk ezért csak {d,f}-et
        * A legolcsóbb kimutató él legyen kék: (b,d) az
    * Hatodik kör
        * {b,c,e} egy kör
        * (b,e) piros
    * Hetedik kör
        * Már csak a-ból nem vezet ki kék él, ezért {a} lesz a csúcshalmaz
        * Amúgy lehet ez a csúcshalmaz akár nem összefüggő is, pl. ha f-ből se vezetne mi kék él, {a,f} is lehetne
        * De most így (a,b) kék
    * Nyolcadik kör
        * {b,c,e,f} egy szép nagy kör piros él nélkül
        * (e,f) piros
    * Kilencedik kör
        * Olyan kör kéne, amiben a (b,c) van..., de ilyet nem fogunk tudni sehogy se csinálni
        * Kék él lenne? Hát persze, mert most még két komponens van
        * Legyen {c,e} az élhalmaz, amiben van kék él, de belőle nem vezet ki
        * Ezzel (b,c) kék

    ```
        K 0        K 3
    a----------b----------c
    \         /|\         /
     \      K/ | \P      /
     P\    1/  |  \4    /K
      6\   /   |   \   /3
        \ /    |    \ /
         d   -1|K    e
          \    |    /
          P\   |   /P
           9\  |  /5
             \ | /
              \|/
               f
    ```

## Prim-algoritmus

### Egy n csúcsú gráfban egy adott csúcs d értéke minimum és maximum hányszor kaphat új értéket a Prim-algoritmus futása során?

* Az elején van egy ciklus minden csúccsal
* Utána ha a "start csúcsot" nézzük, az kap még egyszer értéket, utána ő már "v szerepében" nem lesz, tehát nála ez 2
* Ha nem ezt a csúcsot nézzük, akkor is 2, mert bármelyik végtelenre inicializált csúcs legalább egyszer kell, hogy "v szerepében" legyen, különben nem lenne összefüggő
* A minimum tehát 2
* Képzeljünk el egy olyan gráfot, aminek van olyan u csúcsa, ami minden másik csúccsal össze van kötve. Minden ilyen élsúly különböző, de a gráf összes éleit tekintve a legnagyobbak ezek az élsúlyok. Ha most lezárunk egy adott v csúcsot, ennek az u-nak is frissül (valami pofátlanul nagy értékre) a d-je. Ha most egy w csúcsot zárunk le, és tételezzük fel (w,u) olcsóbb, mint (v,u), akkor ennek megint frissül. Tehát úgy kell a gráfot elkészíteni, hogy garantáltan a (*,u) súlyok csökkenő sorrendjében zárjuk le őket. Ez elég könnyen konstruálható, pl. egy "kezdőcsúcsnak" kinevezett csúcsból ha mindegyikbe megy él, de úgy, hogy amibe a legolcsóbb, abból menjen u-ba a legdrágább, s így tovább, akkor készen is vagyunk. A start csúcsból megy így u-ba a lehető legdrágább él. Tehát n-1 csúcsból megy u-ba, plusz az elején levő inicializálás: ez így n alkalom

### d és π közül valamelyik elhagyható-e?

* A pi-ket elhagyván az algoritmus gondolatmenete nem változna, hiszen az a d értékek alapján választja ki mindig a minimumot, ez alapján lép tovább és ez alapján lesz helyes a működése
* Tehát a d-t valamilyen formában muszáj számon tartani, az egész eljárás végén persze akár ki is dobhatjuk az értékeket
* Viszont a pi-t se érdemes kidobni, hiszen az algoritmus termékét, a minimális költségű feszítőfát a pi-kkel adjuk meg, ennek hiányában nem sok értelme van az algoritmusnak
* Önmagában a d-vel olyasmit tudunk megmondani, hogy egy csúcsot milyen "áron" tudtunk (valahogyan) a teljes rendszerre kapcsolni. De ez is csalóka, hiszen elképzelhető, hogy két csúcs között csak egy él megy, de mindkét csúcs "másik oldalán" még számtalan másik csúcs kapcsolódik hozzá. Tehát egy ilyen d értékkel előfordulhat, hogy egy egész nagy részfát csatlakoztattunk a rendszerre, nem csak egy csúcsot

### Beszéljük meg, milyen implementációnál mit és milyen lépésszámmal csinál a restore() függvény

* A restore() akkor hívódik, amikor egy csúcs prioritása változik, azaz, amikor újra kell prioritizálni a sort
* Azt tudjuk, hogy csak a csúcsot érintő részekkel kell foglalkozni
* A prioritásos sort ábrázolhatjuk rendezetlen tömbbel: ekkor minden remMin() egy lineáris keresés, de cserébe a restore() konkrétan kihagyható, azaz Θ(0) a műveletigénye
* Akár rendezett tömbbel is megadhatjuk a sort, akkor a remMin() konstans (ha bevezetünk még egy indexet, ami mutatja, hogy hogy mekkora a fizikailag n méretű tömb hasznos része. A rendezés pedig n-es, mert mindig egy elemet kell helyre rakni, de ezt potenciálisan gyakrabban hívjuk meg, mint a remMin()-t, előbbit pont n-szer, utóbbit e-től függő módon, ezért ez nem éri meg
* Végül ha kupaccal ábrázoljuk: az inicializálás itt lineáris, mivel az egyik elemnek 0, a többinek végtelen a költsége. A remMin és a restore pedig log(n)-es. Az előbbit max. n-szer, utóbbit max. e-szer végezzük el
* Ebből hasonlóan, mint a Dijkstra-algoritmusnál arra juthatunk, hogy sűrű gráfra jobb a tömbös megoldás, mert az e*log(n) az ott n<sup>2</sup>*log(n) lenne, míg ugyanez mátrixnál és rendezetlen tömbnél csak n<sup>2</sup>. Ritka gráfnál pedig éllistával és kupaccal operálhatunk

### Játszd le a Prim-algoritmust az előző fejezet feladatában megadott gráfra

```
          0          3
    a----------b----------c
    \         /|\         /
     \       / | \       /
      \    1/  |  \4    /
      6\   /   |   \   /3
        \ /    |    \ /
         d   -1|     e
          \    |    /
           \   |   /
           9\  |  /5
             \ | /
              \|/
               f
```

* Inicializálás (f véletlenszerűen választva)
    * minQ: <(f, 0), (a, inf), (c, inf), (e, inf), (b, inf), (d, inf)>
* A minQ-ban d szerint vannak sorbarendezve a csúcsok, d értékeket tüntetem fel a párok második elemeként
* Egyenlőség szerint általában ábécésorrendet alkalmazunk, itt most direkt véletlenszerűen sorolom ilyenkor fel őket

| Csúcs | Pi   | d    | Lezárva |
| ----- |:----:| ----:|:-------:|
| a     | NULL | inf  | nem     |
| b     | NULL | inf  | nem     |
| c     | NULL | inf  | nem     |
| d     | NULL | inf  | nem     |
| e     | NULL | inf  | nem     |
| f     | NULL | 0    | nem     |

* f kivétele a sorból
    * minQ: <(b, -1), (e, 5), (d, 9), (c, inf), (a, inf)>
* Ilyenkor f-et megjelölöm lezártnak, ezek azok, amik már nem a minQ elemei, ezeknek már nem lesz más d és pi értékük, a többi csúcsnak elvileg még javulhat a d-je

| Csúcs | Pi   | d    | Lezárva |
| ----- |:----:| ----:|:-------:|
| a     | NULL | inf  | nem     |
| b     | f    | -1   | nem     |
| c     | NULL | inf  | nem     |
| d     | f    | 9    | nem     |
| e     | f    | 5    | nem     |
| f     | NULL | 0    | igen    |

* b kivétele a sorból
    * minQ: <(a, 0), (d, 1), (c, 3), (e, 4)>

| Csúcs | Pi   | d    | Lezárva |
| ----- |:----:| ----:|:-------:|
| a     | b    | 0    | nem     |
| b     | f    | -1   | igen    |
| c     | b    | 3    | nem     |
| d     | b    | 1    | nem     |
| e     | b    | 4    | nem     |
| f     | NULL | 0    | igen    |

* a kivétele a sorból
    * minQ: <(d, 1), (c, 3), (e, 4)>
* Most nem javult semmi, de később még javulhatnak az értékek

| Csúcs | Pi   | d    | Lezárva |
| ----- |:----:| ----:|:-------:|
| a     | b    | 0    | igen    |
| b     | f    | -1   | igen    |
| c     | b    | 3    | nem     |
| d     | b    | 1    | nem     |
| e     | b    | 4    | nem     |
| f     | NULL | 0    | igen    |

* d kivétele a sorból
    * minQ: <(c, 3), (e, 4)>
* Most se javult semmi

| Csúcs | Pi   | d    | Lezárva |
| ----- |:----:| ----:|:-------:|
| a     | b    | 0    | igen    |
| b     | f    | -1   | igen    |
| c     | b    | 3    | nem     |
| d     | b    | 1    | igen    |
| e     | b    | 4    | nem     |
| f     | NULL | 0    | igen    |

* c kivétele a sorból
    * minQ: <(e, 3)>
* Megérte várni, most e javult

| Csúcs | Pi   | d    | Lezárva |
| ----- |:----:| ----:|:-------:|
| a     | b    | 0    | igen    |
| b     | f    | -1   | igen    |
| c     | b    | 3    | igen    |
| d     | b    | 1    | igen    |
| e     | c    | 3    | nem     |
| f     | NULL | 0    | igen    |

* e kivétele a sorból
    * minQ: <>
    * Ami azt jelenti, vége az algoritmusnak

| Csúcs | Pi   | d    | Lezárva |
| ----- |:----:| ----:|:-------:|
| a     | b    | 0    | igen    |
| b     | f    | -1   | igen    |
| c     | b    | 3    | igen    |
| d     | b    | 1    | igen    |
| e     | c    | 3    | igen    |
| f     | NULL | 0    | igen    |

* A feszítőfa a pi-kből áll össze:

    ```
          0          3
    a----------b----------c
              /|          /
             / |         /
           1/  |        /
           /   |       /3
          /    |      /
         d   -1|     e
               |
               |
               |
               |
               |
               f
    ```

## Kruskal-algoritmus

### Jegyzetből: "Hogyan tudná kifinomultabban értelmezni azt az esetet, ha a Kruskal-algoritmus k > 1 (itt: components > 1) értékkel tér vissza? Mit jelent a k értéke általában? Tudna-e értelmet tulajdonítani a Kruskal-algoritmus által kiszámolt A (itt: mstEdges) élhalmaznak, ha végül k > 1 értéket ad vissza?"

* Ez a változó a komponensek számát, azaz a halmazok számát adja meg, akkor és csak akkor összefüggő a gráf, ha ez a végére 1. Tehát ha nagyobb, mint 1, akkor nem összefüggő, és az összefüggő részgráfjainak száma épp k
* Ekkor egy minimális összköltségű feszítőerdőt adnak meg ezek az élek

### Miért hatékonyabb így, hogy components n-ről indul és csökken, mintha 0-tól indulna és nőne, az élek számát nézné? (kis különbségről van csak szó)

* Az élek száma esetében n-1 lenne az a szám, aminek elérését kéne ellenőrizni, de ehhez minden körben "ki kell számolni", mennyi n-1. Persze az is lehetne, hogy k eleve 1-ről induljon és akkor n-ig mehetünk, de így sokkal érthetőbb és többet mondóbb (lásd előző kérdés) ez a változó

### Milyen gráfban lesz az optimalizáció után továbbra is e-s ciklus a minQ-n való végigjárás?

* Ahol a globális legdrágább él egy 1 fokszámú csúcsot érint, azaz van olyan csúcs, amit a globális legdrágább éllel lehet csak a feszítőfára csatlakoztatni

### "Ez azt jelenti, hogy a fa gyökerének gyerekszáma nő, de a fa magassága csökken(het)." – miért a feltételes mód?

* Legyen {a, b}, {c, d}  és {e, f} három komponens. a, c és e a gyökerek.
    * Ha most az (a,c) élet dolgozzuk épp fel, és mondjuk berakjuk a alá c-t, akkor ezt kapjuk: gyökér: a, gyerekei b és c, c gyereke d. Utána dolgozzuk fel az (a,e) élt: gyökér: a, gyerekei: b, c és e, c gyereke d, e gyereke f. Mivel a gyökereket kötöttük össze, a findSet() nem vitt fel csúcsot. Most, ha bármi miatt findSet(f)-et hívunk, akkor az megtalálja a-t, de beszúrja f-et is a alá. a gyerekszáma nő, de a famagasság nem csökken

### Miért jó/jobb, ha a nagyobb fa alá szúrjuk a kisebbet?

* Ez tudomásom szerint inkább tapasztalati előny, effektíve ami könnyen belátható, az a magasabb fa alá szúrt alacsonyabb szükségessége. A famagasság ezzel nem nő, tehát a keresések nem lesznek hosszabbak

### Feltételezve, hogy a Prim-algoritmus műveletigénye Fibonacci-kupaccal Ο(e + n × log(n)), a Kruskalé pedig Ο(e × log(n)), melyik a jobb választás és miért?

* Szinte mindig a ritka-sűrű esetek szerint kell gondolkodni
* Ha a gráf sűrű, tehát e kb. n^2, akkor a Prim n^2-es, de a Kruskal n^3-ös. Tehát sűrűn a Prim
* Ha e kb. n, akkor a Prim n×log(n)-es, nagyságrendileg a Kruskal is, de az unió-holvan típus birtokában kissé egyszerűbb, illetve nem összefüggő esetben is működőképes

### B-szakirányra: miért nem jó választás terminálófüggvénynek components? Mi működhet helyette?

* Mert nem garantáltan csökken minden ciklusmag-futás után
* A minQ mérete alkalmas, és bár ezt a lekérdezést a MinQ interfész nem tartalmazza, de a terminálófüggvény alapvetően egy elméleti fogalom, tehát ezen nem akadunk fenn, helyességbizonyításhoz ilyen kifejezéseket simán bevezethetünk az algoritmushoz

### Futtasd le a Kruskal-algoritmust az első fejezet feladatában megadott gráfra, jelöld minden körben a komponenseket!

```
          0          3
    a----------b----------c
    \         /|\         /
     \       / | \       /
      \    1/  |  \4    /
      6\   /   |   \   /3
        \ /    |    \ /
         d   -1|     e
          \    |    /
           \   |   /
           9\  |  /5
             \ | /
              \|/
               f
```

* {a}, {b}, {c}, {d}, {e}, {f} a komponensek
* A legkisebb él (b,f), ezért ezt felveszem, és a halmazokat összevonom: {a}, {b,f}, {c}, {d}, {e}
* Lerajzolom reprezentációs szinten is:

    ```
    a    b    c    d    e
         |
         f
    ```

* Most a legkisebb az (a,b) él

    ```
    b    c    d    e
    |\
    f a
    ```

* Most (b,d)

    ```
    b    c    e
   /|\
  f a d
    ```

* Most (c,e) - lehetne (b,c) is, de így érdekesebb

    ```
    b    c
   /|\   |
  f a d  e
    ```
* Most (b,c)

    ```
       b
     /| |\
    f a d  c
           |
           e
    ```

* Nincs tovább, mert immáron egy komponenshez tartozik minden csúcs, de ha mégis folytatnánk (e,b)-vel, akkor a findSet() hívás alatti mókolás miatt így változna az unió-holvanos adatszerkezet:

    ```
        b
     /| | |\
    f a d c e
    ```

* A feszítőfa pedig ugyanaz lett, mint a másik esetben

### Futtasd le a Kruskal-algoritmust a második fejezet példájában megadott gráfra, jelöld minden körben a komponenseket

* Az előzőhöz hasonló módon rajzolom

    ```
    A    B    C    D    E    F
    ```

* Több legolcsóbb él is van, kezdjünk mondjuk a (C,E)-vel

    ```
    A    B    C    D    F
              |
              E
    ```

* (A,B)

    ```
    A    C    D    F
    |    |
    B    E
    ```

* (D,E)

    ```
    A    C    F
    |    |\
    B    E D
    ```

* (B,C)

    ```
      C    F
     /|\
    A E D
    |
    B
    ```

* (A,D) - nincs változás
* (B,D)

    ```
        C    F
     / | | \
    A  E D  B
    ```

* (A,C) - nincs változás
* (B,E) - nincs változás
* (E,F)

    ```
         C
     / | | | \
    A  E D B  F
    ```

* (D,F) - nincs változás
* A feszítőfa pedig azonos lett a Prim-algoritmus anyagában közölttel
