# Gráfok

## Prim-algoritmus

* Minimális költségű feszítőfa-kereséshez egy könnyen implementálható megoldást kínál Prim algoritmusa
* Kijelöl egy random kezdőelemet (":∈" - nemdeterminisztikus értékkiválasztás), és abból iterálva, lokális optimumok szerint mindig egy-egy új csúcsot húz bele a készülő fába. Gyakorlatilag a Dijkstra-algoritmussal azonos a módszer, csak itt nem az utat, hanem a "rendszerbe való beépítés" költségét minimalizáljuk, azaz azt, hogy egy adott még nem elérhető csúcsot, milyen él felvételével tudjuk a legolcsóbban bevenni
    * Ez a piros-kék algoritmusra vonatkozóan csak és kizárólag a kék szabályok ismételgetését jelenti mégpedig olyan "X" halmazzal, ami azokat a csúcsokat tartalmazza, amik nincsenek a minQ-ban. Ez a halmaz mindig eggyel bővül, és mindig azt az élt "színezzük kékre", amelyik a frissen X-be került, azaz "lezárt" csúcsot az eddigi rendszerre köti. n kör van, összesen n-1 élt húzunk be, hiszen amikor X egy elemű, azt az egy elemet még nem volt mire rákötni
    * A tanult, "hivatalos" elméleti háttéralgoritmusra nézvést pedig azt mondhatjuk, hogy a mindenkori vágás a minQ és a V \ minQ elemei között megy (legalábbis az első kör után, amikor már nem üres az egyik halmaz), azaz a már lezárt csúcshalmaz egyre terjeszkedik. A fentihez hasonlóan, mindig a frissen a minQ-ból kikerült elemet a többire rávezető él lesz a könnyű, azaz a hozzáveendő
    * Fontos tehát érteni, hogy nem akkor történik a "kék szabály" vagy a "könnyű él hozzáadása" alkalmazás, amikor frissítjük egy csúcs (d,π)-párját, hanem amikor "jóvá hagyjuk" egy csúcs (d,π)-értékeit akkor, amikor kivesszük azt a minQ-ból
* Implementációjára hasonló megjegyzésekkel élhetünk, mint a Dijkstra esetében

```
prim(g : Graph)
  FOR EACH u ∈ g.V
    u.d := végtelen
    u.π := NULL
  u :∈ g.V
  u.d := 0
  minQ := MinQ()
  initialize(minQ)
  AMÍG !minQ.isEmpty()
    u := minQ.remMin()
    FOR EACH v neighbours(u)
      HA v ∈ minQ
        HA v.d > w(u,v)
          v.d := w(u,v)
          v.π := u
          restore(minQ)
```
