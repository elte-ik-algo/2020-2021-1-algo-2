# Gráfok

## Kruskal-algoritmus

* A minimális költségű feszítőfa kiszámolásának egy másik módszere
* A Prim a csúcsokhoz rendelt értéket, s ezeken ment végig emelkedő sorrendben, egy önkényes kezdeti értékből kiindulva mindig egy-egy csúcsot bevonva a készülő fába
* A Kruskal pedig az éleket osztályozza, mindig a legolcsóbb még feldolgozatlan élről döntjük el, hogy a fa része lesz-e vagy sem
    * Ez egyrészről e-s műveletigényt vetít előre, ami sűrű gráfban nem szerencsés
    * Másrészt pedig mivel az éleket az élsúlyuk szerint rendezzük sorba, ez a sorrend már nem fog változni, ellentétben a Prim-algoritmusban vett csúcs-távolságokra vett prioritásos sorral

```
kruskal(g : Graph)
  mstEdges := ÜRES_HALMAZ
  createSets(g.V)
  minQ := orderEdges(g.E)
  AMÍG !minQ.iSEmpty()
    (u,v) := minQ.remMin()
    HA u.set != v.set
      mstEdges := mstEdges U {(u,v)}
      mergeSets(u,v)
```

* A Prim, ahogy megállapítottuk, egy csúcsból kiindulva növeli egyre a fába bevett csúcsokat
* A Kruskal halmazokat kezel, kezdetben minden csúcs egy önálló halmazt jelent (lehet mondjuk egy tömbbel ábrázolni, aminek indexei a csúcsok, értékei számok - hányas halmaz, kezdetben mind különböző)
* A cél az lenne, hogy halmazokat egyesítsünk, akkor leszünk kész, ha már csak egy halmaz van
    * Egész pontosan a fenti algoritmus ebben a verziójában mindenképp végignézi az összes élt, de nem lenne nehéz egy számlálóval ezen optimalizálni
    * Legrosszabb esetben persze akkor is e-s lesz a ciklus (ha van olyan csúcs, ami csak egy éllel, a globálisan legdrágábbal van a többihez hozzákötve), de átlagban meg fogjuk úszni n körüli lépésszámmal (hiszen a feszítőfa élei és a globális legolcsóbb élek között van valamiféle átfedés)
* Az mstEdges halmazt fogjuk élekkel bővíteni
* a createSets() az élek sorbarendezése egy tömbbe, ez lesz most a "prioritásos sorunk", nem is kell kupac, mert ezt már később nem kell restore-olni. A rendezés nyilván e*log(e) műveletigényű
* A ciklus e-s, kivesszük mindig a minimális élt és eldöntjük két azonos halmazbeli csúcsot köt-e össze
    * Maga a lekérés - első körben legalábbis úgy gondoljuk - konstans
    * Ha azonos a halmaz, nincs mit tennünk, ez az él nem lesz a feszítőfa része, mert korábban már összekötöttük (akár nem is direktben) ezt a két csúcsot egy mégolcsóbb éllel
    * Ha nem azonos, akkor húzzuk be az élt (halmazhoz vétel: konstans), és jelöljük be, hogy ez a két elem, és a halmazuk összes többi eleme is azonos halmazbeli mostantól
        * Na ez viszont azzal jár, hogy végig kell menni a halmazokat megadó tömbön, azaz az e-s ciklusban van egy n-es ciklus, ami sűrű gráfra n<sup>3</sup>, ami elfogadhatatlan

### Unió-holvan (disjoint-set) adatszerkezet

* Erre vezessük be az Unió-Holvan adatszerkezetet, ami a két műveletről kapta a nevét
    * A halmazokat fákként képzeljük el
    * Egy [1..n]-nel indexelt tömböt tartunk nyilván, ahol minden csúcsra az ő szülőjének sorszámát adjuk meg -- ez kezdetben csupa NULL, hiszen minden csúcs külön halmaz, külön fa
    * Amikor megnézzük két halmaz azonosságát (holvan művelet), akkor egészen a NULL-os elemig felmegyünk a fában, és megnézzük az ő azonosítója azonos-e. Ez a famagasságtól függ, ami log(n)
    * Amikor pedig uniózunk, csak beállítjuk az egyik (szerencsés esetben a kisebb - ezt karbantarthatjuk egy változóban!) gyökér szülő pointerét a másik gyökérre -- ez konstans műveletigény
* További javítás, ha minden holvan kéréskor a végigjárt utat behúzzuk a végül megtalált gyökér alá -- ettől laposodik a fa, tehát olcsóbb lesz a holvan
* Az uniózáskor is meg lehet csinálni ezt a lapítást, de akkor kell gyerek pointereket is tárolni, hogy tudjuk, kik azok a csúcsok, akiket be kell húzni a szülő alá

### További optimalizálás

* A prioritásos sor inicializálása az elemek száma szerinti lineáris, azaz itt Θ(e), míg ugyanez tömbbel drágább lenne. A végigjárás ugyan drágább, de az nem domináns művelet
* A halmazok lekérését is kirakhatjuk, bár nem lesz drága művelet, de mégis elég egyszer megcsinálni (alapvetően az uniózásnál újra meg kéne keresni a két gyökeret)
* Illetve a már emlegetett komponens számoló segédváltozót vezethetjük be
* Íme a végső algoritmus:

```
kruskal(g : Graph)
  mstEdges := ÜRES_HALMAZ
  createSets(g.V)
  minQ := orderEdges(g.E)
  components := n
  AMÍG !minQ.iSEmpty() és components > 1
    (u,v) := minQ.remMin()
    uSet := u.set
    vSet := v.set
    HA uSet != vSet
      mstEdges := mstEdges U {(u,v)}
      mergeSets(uSet,vSet)
      components := components - 1
```

## Példa

![Kruskal példa](04_KruskalPelda.png)

* Kezdetben tehát ezek a halmazok adottak: {A}, {B}, {C}, {D}, {E}
* Kiválasztom a legolcsóbb élt: (A,B), mivel A és B külön halmaz, ezért ezt az élt felveszem és A-t és B-t összevonom: {A, B}, {C}, {D}, {E}
* Most a második legolcsóbb: (D,E), ezek is külön vannak, összevonom hát őket: {A, B}, {C}, {D, E}
* Most a (B,C) él jön, ez is két külön halmazt köt össze, ezért felveszem és összevonom az érintett halmazokat: {A, B, C}, {D, E}
* Az (A,C) él a következő, de A és C halmaza azonos, ezért ez nem lesz a legolcsóbb feszítőfa része
* Az (A,D) él véglegesíti a feszítőfát: {A, B, C, D, E}
* Elvileg még a (C,D), a (C,E) és a (B,E) éleket is vizsgáljuk, de már nem lesz változás
* A feszítőfa élei: (A,B), (D,E), (B,C), (A,D)
    * 5 csúcsra 4 él, stimmel