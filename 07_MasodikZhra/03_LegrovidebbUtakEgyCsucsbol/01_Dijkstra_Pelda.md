# Gráfok

## Dijkstra-algoritmus

* Adott egy g = (V, E) gráf és egy s ∈ V csúcsa
* A feladat az, hogy keressük meg minden csúcsra az s-ből kiinduló legrövidebb utat
* A gráf lehet irányított és irányítatlan is
* Lehet ugyan élsúlyozatlan, de ebben az esetben egy sima szélességi bejárás is megadja a megoldást - jobb futási idővel
* Ami fontos feltétel, hogy az élsúlyok ne legyenek negatívak - ez az algoritmus mohó mivolta miatt elvárás
    * Életszerűségileg ez nem nagy megkötés, mert sok olyan valós eset van, amikor ezt a feltételt simán vállalni lehet. Ekkor a Dijkstra-algoritmus a jó választás
    * Formálisan: ∀ e ∈ E : w(e) >= 0

## Példa

* Futtassuk le a Dijkstra-algoritmust az alábbi gráfon, s mutassuk minden lépésben a minQ, d és π értékeket

![Első példa](01_Pelda1.png)

|      | minQ                            | (d,π) | 1        | 2        | 3        | 4        | 5        |
| ---- | ------------------------------- | ----- | -------- | -------- | -------- | -------- | -------- |
| INIT | <(1,0),(2,∞),(3,∞),(4,∞),(5,∞)> |       | (0,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) |
| 1    | <(3,5),(2,10),(4,∞),(5,∞)>      |       | done     | (10,1)   | (5,1)    |          |          |
| 3    | <(5,7),(2,8),(4,14)>            |       |          | (8,3)    | done     | (14,3)   | (7,3)    |
| 5    | <(2,8),(4,13)>                  |       |          |          |          | (13,5)   | done     |
| 2    | <(4,9)>                         |       |          | done     |          | (9,2)    |          |
| 4    | <>                              |       |          |          |          | done     |          |

* A fenti táblázat egyes sorai az egyes körök, melyek során mindig az aktuálisan a minQ elején levő elemet lezárjuk, és az ő szomszédait frissítjük: azt nézzük meg, a szomszéd eddigi regisztrált legrövidebb s-től vett útja hosszabb-e, mint az aktuális csúcson keresztüli út
* Ha igen, akkor frissítjük az adott csúcs d-jét az új távolsággal, és a szülő pointert is az új szülővel
* Az INIT lépés az inicializálás: minden elemet felveszünk a minQ-ba, a startcsúcsot az elejére 0 távolsággal, a többit végtelennel
* Addig megyünk, míg ki nem fogy a prioritásos sor
* A π-k mentén egy feszítőfa is generálódott a gráfból:

![Első példa feszítőfa](01_Pelda1_Feszitofa.png)

## Második példa

* Valaki lefuttatta a Dijkstra-algoritmust egy gráfon, a d értékek így változtak. Rekonstruáljuk minQ-t és π-t történekkel együtt, valamint magát a gráfot

| d    | 1 | 2 | 3 | 4 | 5 | 6 |
| ---- | - | - | - | - | - | - |
|      | 0 | ∞ | ∞ | ∞ | ∞ | ∞ |
|      | 0 | 2 | ∞ | ∞ | 3 | ∞ |
|      | 0 | 2 | 6 | 4 | 3 | ∞ |
|      | 0 | 2 | 5 | 4 | 3 | 9 |
|      | 0 | 2 | 5 | 4 | 3 | 9 |
|      | 0 | 2 | 5 | 4 | 3 | 6 |

* Nyilván a gráfnak 6 csúcsa van, mert itt 6 oszlop van
* A sorok száma is 6, de annak az INIT-tel együtt 7-nek kéne lennie - az első sor biztos az INIT, mert minden elem végtelen, csupán 1 d-je 0, innen következik, hogy ez, az 1-es csúcs kell legyen a startcsúcs
    * A sorok száma pedig azért 6, mert az utolsó sorban már garantáltan nem történik semmi érdemi, ezért a feladat írója elhagyta. Az a sor megegyezik az azt megelőzővel...
    * De itt most a 4-5. sorok is megegyeznek. Ez véletlen csupán, annyit jelent, hogy amikor az 5. sor elemét zártuk le, abból vagy nem volt kimenő él, vagy nagyon drágák voltak, így nem javult semmi. De ez nem jelenti azt, hogy más se javulhat
* A visszafelé gondolkodás menete:
    * Az elején tehát minden csúcs bekerült a minQ-ba, az 1-es 0-val, a többi ∞-nel, π-k is NULL-ok
    * Utána mindig az akt. legolcsóbbat vesszük ki, tehát ha az i. sorban javult egy érték, akkor oda a π-be azt a csúcsot kell beírni ami a előző sorban a legkisebb d-jű volt
    * A gráfba pedig ebből a csúcsba a most frissített csúcs felé vezető élt kell beírni mégpedig a célcsúcs új és a forráscsúcs előző d-jének különbségével, hiszen az új csúcs új d-je a forráscsúcsba való eljutás + a két csúcs közti él hosszával számítódik
* Ezek alapján a teljes táblázat:

|      | minQ                                  | (d,π) | 1        | 2        | 3        | 4        | 5        | 6        |
| ---- | ------------------------------------- | ----- | -------- | -------- | -------- | -------- | -------- | -------- |
| INIT | <(1,0),(2,∞),(3,∞),(4,∞),(5,∞),(6,∞)> |       | (0,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) |
| 1    | <(2,2),(5,3),(3,∞),(5,∞),(6,∞)>       |       | done     | (2,1)    |          |          | (3,1)    |          |
| 2    | <(5,3),(4,4),(3,6),(6,∞)>             |       |          | done     | (6,2)    | (4,2)    |          |          |
| 5    | <(4,4),(3,5),(6,9)>                   |       |          |          | (5,5)    |          | done     | (9,5)    |
| 4    | <(3,5),(6,9)>                         |       |          |          |          | done     |          |          |
| 3    | <(6,6)>                               |       |          |          | done     |          |          | (6,3)    |
| 6    | <>                                    |       |          |          |          |          |          | done     |

* És a gráf - jelölve benne a feszítófa:

![Második példa](01_Pelda2.png)

* Megjegyzés: ilyenkor csak egy minimális lehetséges gráfot tudtunk reprodukálni, lehetnek még benne olyan élek, amik semmit nem javítottak. Pl. az összes s-be menő él ilyen...
