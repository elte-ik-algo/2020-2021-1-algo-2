# Dijkstra-algoritmus

## Első feladat

* Ez egy zh-feladat volt nálam egykoron
* Egészítsd ki az alábbi szöveget egy-egy szóval, szószerkezettel, számmal értelemszerűen
* Indokolni nem kell. Minden helyes és releváns bejegyzés 1 pontot ér
    * A Dijkstra-algoritmus ... (irányítottság), ... (élsúlyozottság) gráfra ad hatékony módszert annak eldöntésére, hogy egy adott kezdőcsúcsából a(z) ... (melyikek) csúcsaiba milyen költséggel és milyen ... (a másik ismérve) lehet a legolcsóbban eljutni. Az algoritmus csak olyan gráfokra működik, ahol minden ... (élre és/vagy csúcsra vonatkozó feltétel). Az algoritmus n csúcsszámú és e élszámú gráfra az inicializálással együtt ... (darab) körből áll. Egy csúcshoz tartozó távolság az inicializálás után legfeljebb ... (darab) alkalommal tud javulni. Ha két egymást követő sorban nincs javulás egyik csúcs távolsága terén se, akkor ... (megállapítás)

### Megoldás

* *Megj.: adtam fél pontokat, de lefelé kerekítettem*
* A Dijkstra-algoritmus irányított vagy irányítatlan, élsúlyozott gráfra ad hatékony módszert annak eldöntésére, hogy egy adott kezdőcsúcsából a gráf csúcsaiba milyen költséggel és milyen csúcsokon keresztül lehet a legolcsóbban eljutni. Az algoritmus csak olyan gráfokra működik, ahol minden élsúly nem negatív. Az algoritmus n csúcsszámú és e élszámú gráfra az inicializálással együtt n+1 körből áll. Egy csúcshoz tartozó távolság az inicializálás után legfeljebb n-1 alkalommal tud javulni. Ha két egymást követő sorban nincs javulás egyik csúcs távolsága terén se, akkor a második körben egy olyan csúcsot zártunk le, amiből nem vezettek ki olcsó élek a még a fába be nem vett csúcsok felé

* Irányítottság: irányított vagy irányítatlan
    * Teljesen mindegy neki. Fél pontokat adtam, ha az egyiket írtátok
* Élsúlyozottság: élsúlyozott
    * Igazából menne súlyozatlanra is, de ott van, hogy "hatékony", és az már nem az, ezért a súlyozatlanra nem adtam pontot, aki vagyolást írt, annak felet
* Melyikek: az összes ("a gráf", "az egyes", a "V-beli", stb...)
    * Felmerültek még a "többi" csúcsok is, de ez nem igazán jó megoldás, nem szabatos, bár a lényeget megfogja - erre fél pontot adtam
    * Az "elérhető"/"összefüggő" 0 pont
    * A "komponens többi" csúcsa valahol a kettő előző keveréke, erre felet adtam
    * "vele összekötött" - tehát direktben egy éllel, ez 0 pontos
    * Volt olyan, hogy "vég"-csúcsok, tehát az utak végei. Ez 0 pont, mert épp a bejárás a lényeg. Nem is lehet megmondani, mi számít út végének
    * Igazából a start csúcsra is megad egy üres utat, és a nem elérhető csúcsokról is kiderül, hogy azok (nem akasztja meg az algoritmust, ha van ilyen)
* Másik ismérv: úton (esetleg szülőn át)
    * A π ezt adja meg. Az "úton" jobban tetszett, hiszen bár a π csak a közvetlen szülőt adja meg (azaz milyen "élen" jutok el az adott csúcsba), de értelmileg ez az egész utat jelenti
    * Felmerült még, hogy "hatékonysággal"/"távolsággal", de az ugyanaz, mint a "minimális költséggel"
* Feltétel: élsúly nem negatív
    * A pozitív már nem hibátlan - fél pont
    * Sokan írták, hogy "minden csúcs elérhető" - ez abszolút nem igaz, lásd feljebb
    * Az se jó, hogy "élsúly természetes szám", mert akkor nem lehetne tört... :)
* Körök száma: n+1
    * Inicializálás, illetve ott berakok minden csúcsot egy prioritásos sorba
    * Ezt ürítem ki, minden körben egy elemet
    * Tehát inicializálás + n kör, összesen n+1
    * Itt csak a pontos választ fogadtam el
* Javulások száma: n-1
    * Ez trükkös volt, ugye van n+1 kör, maga az inicializálás kiesik, mert hát az inicializál (egyet 0-ra - de az nem tud hova javulni, a többit végtelenre), illetve az utolsó kör is kiesik, mert ott már csupa feltárt csúcsba vezethet csak él, így tehát n+1-2 = n-1 kör van, ahol javulhat a súly
    * Persze ez maximum-szám, és lehet ezt másképp is megközelíteni, arra is megadtam a pontot, hogy "maximum ahány él kivezet belőle", hiszen ezen csúcsok feldolgozásakor van esélye javulni
    * Itt n-2-re még megadtam a fél pontot, mert könnyű volt elszámolni, elfelejteni, hogy az inicializálás is benne van
* Ha nincs javulás: igazából akkor nincs semmi... :)
    * Az a lényeg, hogy ez (az utolsó sor kivételével) nem jelenti azt, hogy vége az algoritmusnak, megtalálta az optimumot
    * Ez azt jelenti, hogy az adott, feldolgozott csúcs csupa lezárt csúcsba vezető élekkel bírt, esetleg semmilyen éllel, vagy ami a legvalószínűbb, drága, a fába be nem kerülő élekkel rendelkezett
    * Tehát abban a körben egyik csúcs értéke se javult (maga a lezárt csúcsé se, de az sose javul a lezárása körében, amiatt zárhatjuk le, hogy minimális), abban a körben nem talált jobb utat
    * Viszonylag semmitmondó válaszokra (pl. "tovább megy az algoritmus") is megadtam a pontot, aki azt írta leáll, annak nem

## Második feladat

* Ez egy konzultáción hangzott el ebben a formában
* Írjuk algoritmust az alábbi problémára
    * Adott egy gráf, aminek a csúcsai holland városok, az élei pedig a városok közvetlen úton vett távolságai (a gráf irányított mert lehetnek útlezárások, stb.). Állapítsuk meg, hogy az egyes városok milyen távolságra vannak Utrechtből kiindulva (a lehető legrövidebb úton)
    * Az, hogy holland azért fontos, mert viszonylag sűrű az úthálózat
    * Ábrázolásszinten írjuk meg, kivéve a "done" halmazt

### Megoldás

* Dijsktra alkalmazható, mert nincs negatív élsúly
* Mivel sűrű a gráf, ezért mátrixos gráfábrázolást, és rendezetlen tömbös sorábrázolást választunk
* A mátrix legyen a városnevekkel indexelve, amit használhatunk így:
    * Deklaráció: t : T[string]
	* Indexelés - konstans: t["a"]
	* Végigjárás a kulcsokon - lineáris: FOR EACH k eleme t.getKeys(), ciklusmagban: t[k]
	* t[3] is használható, és ezzel megkapjuk a reprezentációszintű 3. elemet...

```
distancesFromUtrecht(cities : R U {végtelen}[String,String], d : R U {végtelen}[String])
	minQSize := cities.keys().size()
	FOR k eleme cities.keys()
		d[k] := végtelen
		minQ[k] := végtelen
	d["Utrecht"] := 0
	minQ["Utrecht"] := 0
	done := ÜRES
	AMÍG minQSize > 0
		minCity := searchMin(minQ, minQSize)
		minQ[minCity] := minQ[minQSize]
		minQ.keys()[minCity] := minQ.keys()[minQSize] //itt frissitem ugymond a kulcsot is
		minQSize := minQSize - 1
		done := done U {minCity}
		FOR EACH j eleme cities.keys()
			HA j nem eleme done és d[j] > d[i] + cities[minCity][j]
				d[j] := d[minCity] + cities[minCity][j]
				minQ[j] := d[j]
```

* Ebből a példából is jól látható, milyen szerencsés is az OOP elveit követni. Itt most nagyon nem követjük, ezért a kód feleslegesen el lett bonyolítva
