# Gráfok - Legrövidebb utak egy forrásból

* Erre órán már nem maradott idő, ezért utólag pótlom
* Kollégám, Vadász Péter készítette jegyzet ebben a mappában megtalálható dijkstra.pdf néven
* Az alábbi youtube-linken érhető el a doksiról készült "unboxing videó": https://youtu.be/GutUwZ7CYEY
    * Ebben a mappában a 00_MagyarazatJegyzet.txt fájl tartalmát használtam mankóként a süketelésem során

## Dijkstra-algoritmus

* Lásd PDF és videó
* Erről van anyagom is, lásd 01_Dijkstra_Pelda.md, 02_Dijkstra_Algoritmus.md és 03_DijkstraFeladatok.md

## Legrövidebb utak DAG esetén

* Lásd PDF és videó

## Bellman--Ford-algoritmus

* Lásd PDF és videó
