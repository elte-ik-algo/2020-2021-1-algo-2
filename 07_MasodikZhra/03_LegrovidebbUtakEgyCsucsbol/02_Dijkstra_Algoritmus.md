# Gráfok

## Dijkstra-algoritmus

* Az algoritmus absztrakt esetre:

```
dijkstra(g : Graph, s : Vertex)
  FOR EACH u ∈ g.V
    u.d := végtelen
    u.π := NULL
  s.d := 0
  minQ := MinQ()
  done := ∅
  initialize(minQ)
  AMÍG !minQ.isEmpty()
    u := minQ.remMin()
    done := done ∪ {u}
    FOR EACH v neighbours(u) \ done
      HA v.d > u.d + w(u,v)
        v.d := u.d + w(u,v)
        v.π := u
        restore(minQ)
```

## Reprezentáció és műveletigény

* Milyen gráf- és prioritásos sor megvalósítást érdemes használni?
* Ez, mint látni fogjuk, attól függ, sűrű-e vagy ritka a gráf
* INIT szakasz:
    * Az első ciklus mindenképp Θ(n)-es
    * A következő 3 sor Θ(1)
    * Az init hívás során berakjuk a prioritásos sorba az összes elemet
        * A prioritásos sort reprezentálhatjuk kupaccal, akkor adná magát, hogy ez egy n×log(n)-es művelet legyen, mert n elemet kell berakni, és emelni. De nem, ennél olcsóbb, csak n-es, mivel tudjuk, hogy a kupac teteje az s lesz 0-val, a többi pedig tetszőleges sorrendben ∞-nel, tehát különösebben számolgatni nem kell, csak egy ciklussal feltölteni a kupac mögöttes tömbjét
        * A kupac közelről nézve végülis egy rendezetlen (vagyis közel rendezetlen) tömb, de kissé elsőre talán meglepő módon csinálhatjuk ezt konkrétan rendezetlen tömbbel. Ekkor a feltöltés szintén n-es, de ez nyilvánvaló: csak random bepakolgatjuk az elemeket
* A csúcsok feldolgozása
    * A külső ciklus mindennek ad egy n-es szorzót, hiszen kezdetben minden csúcs a minQ eleme, és minden körben egyet veszünk ki
    * Maga az isEmpty() vizsgálat minden esetben konstans
    * Újabb MinQ-művelet következik:
        * A remMin() rendezetlen tömb esetén egyszerű ügy: egy minimumkiválasztással (vagy ha úgy tetszik feltételes minimumkereséssel) végig kell mennünk az elemeken, amik még nincsenek kész, és kiválasztani a legkisebbet. Elviekben ez egyre kisebb gyűjteményen való keresés, a gyűjtemény mérete n-től függ, és ez az egész bír egy n-es szorzóval, innen ez: Ο(n<sup>2</sup>)
        * A kupacos megvalósításnál is n-szer kell elvégezni a kivételt, és minden lépéskor süllyesztéssel helyreállítani a kupacot, így ez: Ο(n×log(n))
    * Halmazba uniózás önmagában tekinthető konstansnak
    * A következő ciklus egy Ο(e)-s ciklus (mivel a done-ba menő éleket nem nézzük, így nem nézzük minden csúcs minden kimenő élét)
    * A restore()-on kívül minden konstans
        * Az pedig megint implementációfüggő, rendezetlen tömbnél konkértan a SKIP, tehát konstans
        * MinQ esetén pedig mivel a v értéke csökkent, ezt kellhet emelni, ami egy log(kupacméret)-es művelet, így a szorzóval együtt: Ο(e×log(n))
* Hol tartunk most?
    * Vannak fix, reprezentációtól független költségek
        * Az inicializálás Θ(n) + Θ(1) + Θ(n) - bár itt az utolsó tétel nem független a reprezentációtól, csak éppen ugyanarra jött ki
        * A csúcsok bejárására rendezetlen tömb esetén Ο(n<sup>2</sup>) + Ο(e) jön ki; kupac esetén pedig Ο(n×log(n)) + Ο(e×log(n))
* Mivel a műveletigény a csúcsok (n) és az élek (e) számától is függhet, érdemes rögzíteni, hogy az adott gráf sűrű (azaz e ~ n<sup>2</sup>) vagy ritka (e ~ n), nézzük meg mire jönnek ki ezek a műveletigények e-t megfelelően behelyettesítve:

|       | Tömbös                  | Kupacos                        |
| ----- | ----------------------- | ------------------------------ |
| Sűrű  | Θ(n) + Ο(n<sup>2</sup>) | Θ(n) + Ο(n<sup>2</sup>×log(n)) |
| Ritka | Θ(n) + Ο(n<sup>2</sup>) | Θ(n) + Ο(n×log(n))             |

* Azaz sűrű gráfra bizony hatékonyabb az egyszerűbb rendezetlen tömbös MinQ-megvalósítás, s persze ekkor a gráfot is érdemes mátrixosan megadni
* Ritka gráfra pedig az éllistás gráf- és a kupacos MinQ-megvalósítás a nyerő
* Van még egy implementációs trükk, amiről eddig hallgattunk:
    * Amikor a kupacot helyre kell állítani, akkor tudjuk, hogy "v" d-je romlott el, tehát azt kell frissíteni, emelni a kupacban
    * De honnan tudjuk, melyik a v? A kupac egy tömb a lelke mélyén, ami nem azon az indexen tárolja v-t, mint a gráfot reprezentáló éllista vagy mátrix tömbje
    * Azaz minden ilyen helyreállítást meg kéne előzzön egy n-től függő méretű gyűjteményben való keresés
    * Ezt elkerülendő (hiszen ez egy e-s ciklusban szorzódna hozzá a teendőinkhez) tartsunk ilyenkor számon egy segédtömböt, mely a csúcsokkal van indexelve, értékei pedig az adott csúcs a heap-reprezentációbeli indexe (ezzel persze bizonyos programozási alapelvekre fittyet hányunk, de mégis így tudjuk elérni ezt a jónak mondható hatékonyságot)
* Plusz optimalizáció lehetséges, ha az elején nem minden csúcsot veszünk fel a minQ-ba, hanem csak s-t; de ez esetfüggő, hogy megéri-e

## Mohóság

* A Dijkstra-algoritmus mohó
    * Ez azt jelenti, a globális megoldást a lokális megoldás mentén keresi. Azaz, ha épp u csúcsot vizsgáljuk és oda v-ből olcsóbb eljutni, mint w-ből, akkor u-nak v lesz a megelőző csúcsa. Az algoritmus során mindig vesszük az aktuálisan legalacsonyabb d-vel rendelkező csúcsot és ebben a körben az ő szomszédjainak van esélye arra, hogy rajta keresztül "olcsóbbá" váljanak. De a feldolgozás alatt álló (és a már feldolgozott) csúcsoknak nem. Ez a mohóság: "ami épp most a legjobbnak tűnik, azt konzerválom ebben az állapotban és az alapján keresem a legjobbat" - azaz aki egyszer kikerült a sorból már nem kerül vissza
    * Ez a mohó stratégia működőképes ha a gráfban nincs negatív él. Tehát a Dijkstra algoritmus előfeltétele az, hogy ne legyen negatív élköltség
    * Ha van negatív él, akkor előfordulhat, hogy két különböző u és v közötti negatív útnál a hosszabb út rövidebb, mint a rövidebb egy prefixe. Ezáltal a v-t hamarabb terjesztjük majd ki, mint ahogy felfedeznénk a rövidebb utat
* Kérdés:
    * Ha a minQ-t (és magát a típusát) kicserélem maxQ-ra ugyanúgy a d alapján
    * Ha az algoritmusban a kacsacsőröket megfordítom
    * Ha a plusz végtelen helyett mínusz végtelent írok
* Akkor vajon jó-e legnagyobb költségű utak meghatározására?
* Válasz: Nem, és ugyanazért nem, amiért nem lehet negatív él
    * Tekintsük az alábbi példát:

    ![Ellendijkstra-ellenpélda](02_Dijkstra_Ellenpelda.png)

    * Itt előbb A-t vesszük, emiatt B d-je 5 lesz. Most B-é a legnagyobb, ezért C-é 11, D-é 25 lesz. Most D a legnagyobb, ezért E-é 26 lesz, C-é marad 11, ami kisebb ennél, tehát D-vel folytatjuk, ami E-t 26 súlyúvá teszi. Most E súlya még mindig több, mint C-é, ezért F súlya E-n keresztül alakul, és mivel ez is több, mint C-é, ami "lokális minimumban" ragadt, ezért F-et le fogjuk zárni mielőtt C-ből megnéztük volna az utat. Ami amúgy hosszabb lett volna
    * A fenti probléma feloldására vezetjük be a Bellman--Ford-algoritmust
