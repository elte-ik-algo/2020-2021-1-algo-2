* Itt nem készült külön videós anyag, viszont a W08, azaz november 4-ei órán az itteni anyagon mentünk végig. Íme róla a [videó](https://youtu.be/XKgHHQiY_qY)
* A mutatott online szemléltető eszköz: https://www.cs.usfca.edu/~galles/visualization/BPlusTree.html
    * Vigyázat, a degree alapértelmezésben 3, nem 4!
