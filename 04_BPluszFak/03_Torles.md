# B+-fa

* A törlés (főleg a speciálisabb esetei) várhatóan kisebb súllyal fog szerepelni a zh-kban, vizsgákon, de azért érdemes alaposan átnézni ezt a részt is
* Induljunk ki az előző rész fájából:

```
                  11/17
                 /   |  \ 
               /     |    \
             /       |      \
           /         |        \
         /           |          \
     6/8             13/15        19
    / | \           /  |  \       |  \
   /  |  \         /   |   \      |    \
4/5  6/7  8/10  11/12 13/14 15/16 17/18 19/20
```

## Törlés

* Végezzük el rendre az alábbiakat (d - törlés, i - beszúrás): <d-10, d-5, d-6, d-11, d-8, i-21, d-17, d-15, d-20>
* d-10
    * Levelekben, ahogy korábban megállapítottuk, ugyanannyi kulcs van, mint pointer; míg a többi csúcsban eggyel több pointer, mint kulcs
    * A pointerek száma mindenhol legalább alsóegészrész(d/2) kell legyen - ami itt 2
        * Kivéve ha egy csúcs van
    * A 10 törlésénél az első lépés a levél megkeresése, előbb balra megyünk, mivel 10 < 11, aztán jobbra, mivel 10 >= 8
    * Meg is érkeztünk, töröljük a 10-et
    * Ekkor "8/10" most már csak "8", azaz egy pointere maradt, ami pazarlás, ezt orvosolni kell
    * Először a testvérértől átvenni próbálunk (6/7 a testvére, 11/12 csak "unokatestvér"!), de ha ezt tennénk, akkor a "6"-os maradna egyke, nem lennénk előrébb
    * Ezért, mivel ezt nem tehetjük meg (jelen esetben nincs 3 pointeres testvére), úgy oldjuk meg a problémát, hogy az egyik testvérével összevonjuk (testvére mindenképp van, különben a szülő lenne egy pointeres, ami típusinvariánsba ütközne)
    * Attól se kell félnünk, hogy ő és a testvére együtt túl sok pointeres lenne, hiszen a testvérének épp alsóegészrész(d/2) pointere van (különben átvettünk volna tőle), neki pedig eggyel kevesebb, tehát együtt se adják ki a d-t - levélben max. d-1 pointer lehet
    * Tehát összehúzzuk őt a testvérével, ebből lesz a 6/7/8 levél
    * Most a szülőbe propagálunk: ha összevonás volt, az azt jelenti, hogy eggyel kevesebb gyerek lett, azaz eggyel kevesebb hasítókulcsra is van szükség
    * Mivel a 6/7 és 8/10 levelek közötti kulcs a 8-as volt, ezért ez létjogosultságát veszti: egyszerűen kitöröljük ilyen esetben
    * Most egy kulcs, azaz két pointer maradt a köztes csúcsban, ez valid mennyiség, ezért az algoritmus leáll:

```
                 11/17
                / |  \ 
              /   |    \
            /     |      \
          /       |        \
        /         |          \
      6          13/15        19
    / |         /  |  \       |  \
   /  |        /   |   \      |    \
4/5  6/7/8  11/12 13/14 15/16 17/18 19/20
```

* d-5
    * 5 < 11, ezért balra megyünk, 6-nál is kisebb, ezért újra. Töröljük
    * Egy pointeres marad a levélcsúcs, mivel a szomszédjának 3 van, ha elveszünk egyet, még úgy is marad, több, mint 1, ezért ez fog történni
    * Ekkor is van dolgunk a szülővel: mivel a jobb gyerek legkisebb eleme átment, ezért a hasítókulcsot ennek megfelelően frissíteni kell a jobb gyerek újdonsült legkisebb kulcsára

```
                 11/17
                / |  \ 
              /   |    \
            /     |      \
          /       |        \
        /         |          \
      7          13/15        19
    / |         /  |  \       |  \
   /  |        /   |   \      |    \
4/6  7/8   11/12 13/14 15/16 17/18 19/20
```

* d-6
    * Mivel 6 kisebb, mint 11, majd 7, ezért a legbaloldalibb levél az érintett
    * Töröljük innen a 6-ost, így csak egy pointer maradt, valamit tenni kell
    * Átvenni nem tudunk, mert akkor a 8-as maradna egyedül, így tehát egyesülni kell: 4/7/8 az új levél
    * Felmegy a hatás a szülőbe: a 7-es már nem hasító kulcs - kiesik. Marad egy k=1-es köztes csúcs - azaz 0 kulcs, 1 pointer. Ez nem szabályos, ezzel is kezdeni kell valamit
    * Az ő szomszédja a "13/15", aminek 3 pointere is van, tehát ha egyet elveszt, akkor is legalább alsóegészrész(d/2) marad, így hát a 11/12-es levélbe mutató pointer átmegy
    * Lássuk, most hogy állunk (zh-n ezt a köztes állapotot nem kell lerajzolni):

```
                11/17
               / |   \ 
             /   |     \
           /     |       \
         /       |         \
       /         |           \
      ?          ?/15        19
     / \        /    \       |  \
    /   \      /      \      |    \
4/7/8  11/12  13/14  15/16  17/18  19/20
```

* Hogy frissülnek ilyenkor a hasítókulcsok?
* A 7-es, mint mondtuk kiesik (mivel eggyel kevesebb levél lesz, nyilván a hasítókulcsok száma is csökken)
* A többi esetben frissíteni kell, hiszen az átpointerezések miatt a hasítókulcsok nincsenek összhangban a valósággal
* Vigyázat, ilyenkor NEM újraszámoljuk a kulcsokat, tehát nem egyszerűen csak annyi a szabály, hogy mivel a 11-es az első ? jobboldali legkisebb eleme, ezért az 11 lesz, stb. (nagy d-kre és nagy n-ekre ez feleslegesen sok munka lehetne)
* Ezzel szemben a már meglevő kulcsok fognak "mozogni"
* Kezdjük a második ?-lel a 15 előtt, ami eddig 13 volt
* Mivel eddig 3 gyerek volt, most meg már csak 2, ezért itt elég egy hasítókulcs. Mivel csak a legbaloldalibb elemet vesztette el, a többi elem viszonya nem változik, ezért a 15 marad itt az egyedüli kulcs
* De a 13 sem vész el! Menjünk tovább a szülőhöz, ami most a 11/17. Itt a 2. és 3. részfa nem változott, tehát a köztük differenciáló 17 marad
    * De az első és a második változott, hiszen a második legkisebb része átment az elsőbe. Eddig 11-tól már a gyökér második részfájába kerültünk, most pedig már csak 13-tól! Ezért a 2. gyerek 13-as hasítókulcsa ide jön fel. Világos miért: átkerült az a rész, ezért a 2. részfában csak az annak az eredetileg 2. részfájától kezdődő részek maradtak, azok pedig 13-tól kezdődnek
* Az első kérdőjel pedig 11 lesz, hiszen ami eddig a második részfa legkisebb eleme volt, az most itt a nagyobbik elem - és a hasítókulcs épp ezt jelenti
* Nagyon fontos tehát, hogy a kulcsokat nem a józan ész szerint újraszámoljuk, hanem a kulcsok mozognak a Node-ok között! Ebben a helyzetben ez a kettő épp ugyanarra jön ki, de ez nem szabály, hanem kivétel
* Röviden:
    * A 7-es kiesik, mert egy levél kiesett
    * A 11-es lemegy a gyökérből, kvázi a 7-es helyére, mert most már a 11, ami eddig a gyökér 2. részfáját adta meg, lett itt a 2. részfa
    * A 11-es helyére felmegy a 2. részfa 13-asa, mert ami eddig a 2. részfa 2. részfája volt, az most már maga a 2. részfa (és annak az első részfája)
    * A 13-as helyre nem került semmi, mert ott csökkent a részfák (pointerek) száma

```
                13/17
               / |   \ 
             /   |     \
           /     |       \
         /       |         \
       /         |           \
      11         15          19
     / \        /  \         |  \
    /   \      /    \        |    \
4/7/8  11/12  13/14  15/16  17/18  19/20
```

* d-11
    * 11 < 13, ezért balra megyünk, majd 11>=11, ezért jobbra
    * Kitöröljük, marad egy pointer, ezért meg kell néznünk, át tudunk-e venni valamit
    * Igen, a 8-ast a szomszédból
    * Lett tehát egy 4/7 és egy 8/12-es levél, köztük a hasítókulcs már nem helyes, ezért ezt frissítjük a jobb gyerek legkisebb elemére: 8-ra

```
               13/17
             / |    \ 
           /   |     \
         /     |      \
       /       |       \
     /         |        \
    8         15         19
   /\        /  \        |  \
  /  \      /    \       |    \
4/7  8/12  13/14  15/16  17/18  19/20
```

* d-8
    * 8 < 13, ezért balra, 8 >= 8, ezért jobbra
    * Kitöröljük, 12 egyedül marad, de nem vehet el a testvérétől, mert akkor a 4 maradna egyedül
    * Ezért egyesülnek 4/7/12-ként
    * Ekkor a szülőben 8 már nem egy értelmes hasítókulcs, kiesik
    * Ekkor ez a Node, amiben eddig 8 volt, már csak egy pointert szül, így nincs létjogosultsága
    * A testvére szintén egy kulcs-két pointeres, ha elvennék egy pointert, ő maradna egyedül, így ez se megoldás: egyesíteni kell őket
    * Tehát a 4/7/12, a 13/14 és a 15/16 szülője egyesül
    * Viszont akkor ezek szülője felé (ami a gyökér) is jeleznünk kell: ott most eggyel kevesebb gyerek lett, hiszen 2-t összevontunk
    * A 17-es kulcs a gyökérben teljesen korrekt, a változatlan jobb gyerekre utal
    * A 13-as biztos, hogy eltűnik onnan, mert már csak 2 gyereke van
    * A "13/14 és 15/16 szülője" csúcs eddig csak egy 15-ös kulcsot tárolt. Ez teljesen korrekt, mivel a két levél között ez mutatja az utat
    * De most balról bekerült a 4/7/12 is, tehát muszáj még egy hasítókulcsot kapnia. Ez az új gyerek eddig a gyökér legkisebb elemében volt, azaz a 13-as kulcs "bal oldalán". Most ugyanúgy ott van csak egy szinttel lejjebb
    * Konklúzió:
        * A 8-as kiesik
        * A 15-ös marad, de jön mellé balról még egy kulcs, hiszen jött mellé balról, még egy pointer - ez a kulcs a 13, hiszen az a részfa jött hozzá, aminek eddig is az volt a kulcsa
        * A gyökér 13-asa lemegy tehát a bal gyerekébe, helyére nem kerül semmi, mert csökkent a gyerekszám

```
                 17
               /    \ 
              /      \
             /        \
            /          \
       13/15            19
     /  |   \           | \
    /   |    \          |  \
   /    |     \         |   \
4/7/12  13/14  15/16  17/18  19/20
```

* i-21
    * Elmegyünk legjobbra és beszúrjuk
    * Mivel nem volt érintve semmilyen határoló kulcs, ezért nem változik semmi
```
                 17
               /    \ 
              /      \
             /        \
            /          \
       13/15            19
     /  |   \           | \
    /   |    \          |  \
   /    |     \         |   \
4/7/12  13/14  15/16  17/18  19/20/21
```

* d-17
    * Mivel 17 >= 17, ezért jobbra megyünk
    * 19-nél kisebb, ezért balra
    * Kitöröljük, marad 18, ami most tud lopni a jobb testvérétől, lesz tehát egy 18/19 és egy 20/21-es csúcs
    * A szülőben a hasítókulcsot frissíteni kell
    * De annak szülőjében nem! Így eljutottunk oda, hogy lett egy olyan eset, hogy egy hasítókulcshoz nem tartozik valódi kulcs!
```
                 17
               /    \ 
              /      \
             /        \
            /          \
       13/15            20
     /  |   \           | \
    /   |    \          |  \
   /    |     \         |   \
4/7/12  13/14  15/16  18/19  20/21
```

* Kitérő: Mi lenne ha most szúrnánk be a 17-et?
    * A hasítókulcsok ugyanúgy működnek: 17 >= 17, ezért jobbra; majd 17 < 20, ezért utána balra megyünk, a 18 elé pedig beszúrjuk. És kész vagyunk
* Kitérő2: Mi lett volna, ha törlés előtt akarjuk beszúrni?
    * Elmegyünk a hasítókulcsok segítségével ugyanúgy a megfelelő levélbe; mivel ott jelen van, és minden elem csak egyszer szerepelhet, nem szúrjuk be
* Kitérő3: Mi lenne, ha most megint törölnénk 17-et?
    * Megkeressük a helyét, a 18/19-es az. Mivel ott nincs, nem töröljük
* d-15
    * 15 < 17, ezért balra; majd 15 >= 15, ezért jobbra megyünk
    * Töröljük, 16 egyedül marad, lopni nem tud, mert 13/14 is csak két pointeres
        * Megjegyzés: ha d-14 lett volna, akkor tudott volna átvenni kulcsot a bal szomszédjától
    * Tehát jobb híján egyesülnek a kulcsok a 13/14/16-os levélbe
    * Ez felmegy a szülőbe is, hiszen eggyel kevesebb gyereke lett, frissíteni kell a kulcsokat. Kiesik a 15-ös (nem azért, mert a 15-öt töröltük, hanem mert az ő leveléből töröltünk)
    * Mivel egy kulcs két pointert jelent a levélszint fölött (vagy számszerűleg alatt...), ezért itt meg is tudunk állni

```
             17
           /    \ 
          /      \
         /        \
        /          \
      13           20
    /   \          |  \
   /     \         |   \
  /       \        |    \
4/7/12  13/14/16  18/19  20/21
```

* d-20
    * 20 > 17, ezért jobbra, saját magánál se kisebb, ezért újra jobbra
    * Töröljük, mivel a testvérnek nincs elég pointere, egyesülnek 18/19/21-be
    * Szülőből ezért a 20-as mint hasítókulcs kiesik, a testvérétől nem tud venni: hát egyesül vele
    * A szülő szülőjéből leesik a 17-es, de most nem megy fel a helyébe semmi, mert csökkent a gyerekszáma
    * Konkrétan egy gyereke (és 0 kulcsa) van a gyökérnek, ezért kiesik (mert neki se testvére, se semmi), csökken a fa magassága

```
          13/17
         /  |  \
        /   |   \
       /    |    \
      /     |     \
4/7/12  13/14/16  18/19/21
```

* Tanácsok: érdemes előbb a leveleket újra leírni az összevonások (beszúráskor kettéválások) után. Ezek után érdemes lehet a nem érintett csúcsokat visszarajzolni, illetve az érintett csúcsok új szülő-gyerek viszonyait. Ezek után a kulcsokat már viszonylag nem nehéz kitalálni
