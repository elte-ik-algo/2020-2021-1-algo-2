# Gráfok

## Reprezentációk

* Tekintsük példának az alábbi irányítatlan...

![Irányítatlan gráf](02_iranyitatlan.png)

* ... és irányított gráfot:
 
![Irányított gráf](02_iranyitott.png)

* Kétféle számítógépes ábrázolási lehetőségünk nyílik: a szomszédsági mátrixos és az éllistás
	* Mátrix:
	    * Ez egy n-szer n-es, azaz négyzetes mátrix
		* A mátrix i. sorának j. eleme jelenti az i. és a j. (címkéjű) csúcs közötti élet (tehát az i. a forrás- és a j. a célcsúcs)
		* c-vel, esetleg w-vel szoktuk jelölni a mátrixot
	* Éllista:
		* Adott egy n elemű tömb, aminek minden eleme egy csúcsot jelent. Minden csúcsra rá van aggatva egy lista, amiben szintén csúcsok vannak, azok, amelyikbe az adott csúcsból élek vezetnek
		* adj-gyal jelöljük az éllista tömbjét
* Mi van ha a gráf irányított/irányítatlan?
	* Irányítatlan:
		* A mátrix szimmetrikus: az i. sor j. eleme és a j. sor i. eleme ugyanazon élről szól (lehet akár csak az alsóháromszög mátrixszal foglalkozni)
		* Az i. csúcs éllistájában is szerepel a j és a j. csúcs éllistájában is szerepel az i
	* Irányított:
		* A mátrixban ha az i. sor j. elemében van bejegyzés, akkor az i. csúcsból vezet a j-edikbe él. A fordított irányról ez a cella nem szól
		* Ha az i. éllistában szerepel a j-vel jelölt node, akkor i-ből vezet j-be él. A fordított irányról ez a node nem szól
* Mi van ha a gráf élsúlyozatlan/súlyozott?
	* Élsúlyozatlan:
		* A mátrixban bitek vannak. A 0 jelenti, hogy nincs él, az 1, hogy van
		* Az éllisták node-jai párok: egy a célcsúcs címkéje, és a next pointer
	* Élsúlyozott:
		* A mátrixban valós számok vagy ∞ van. A ∞ jelenti azt, hogy nincs él, a valós szám pedig az él súlya
		* A node-ok hármasok, az eddigiekhez képest az új adattag jelenti az élsúlyt. Ez valós szám, ha nincs él, nincs node (∞ nem szerepel)

* Példák
    * A fenti irányítatlan gráf mátrixa

    | cs | 1. | 2. | 3. | 4. |
    | -- | -- | -- | -- | -- |
    | 1. | 1  | 1  | 0  | 0  |
    | 2. | 1  | 0  | 1  | 0  |
    | 3. | 0  | 1  | 0  | 0  |
    | 4. | 0  | 0  | 0  | 0  |

    * Listája
	
    | cs |        élek        |
    | -- | ------------------ |
    | 1. | (1, →), (2, NULL) |
    | 2. | (1, →), (3, NULL) |
    | 3. | (2, NULL)          | 
    | 4. | NULL               |

    * A fenti irányított gráf mátrixa
	
    | cs | 1. | 2. | 3. | 4. |
    | -- | -- | -- | -- | -- |
    | 1. | 1  | 1  | 0  | 0  |
    | 2. | 0  | 0  | 1  | 0  |
    | 3. | 0  | 1  | 0  | 0  |
    | 4. | 0  | 0  | 0  | 0  |

    * Listája
	
    | cs |        élek        |
    | -- | ------------------ |
    | 1. | (1, →), (2, NULL) |
    | 2. | (3, NULL)          |
    | 3. | (2, NULL)          | 
    | 4. | NULL               |

* Mikor melyik ábrázolást érdemes használni?
	* Mivel a mátrix garantáltan n×n-es méretű, ezért ritka gráfok esetén (ahol az élek száma nem nagyobb nagyságrendileg a csúcsok számánál) ez pazarló, itt tehát általában az éllista a jobb
	* Sűrű mátrixnál (ahol az élek száma korrelál a csúcsok száma négyzetével) így is úgy is négyzetes a költség, tehát itt inkább megéri a mátrix, hiszen gyorsabb elérni egy-egy elemet, és nem kell "feleslegesen" tárolni a next pointereket
