# Konzultációs feladatok BFS témában

## W11 (2020.11.25.) 3. feladata

* Döntsük el irányítatlan, éllistával megadott gráfról BFS-re alapuló algoritmussal, hogy fa-e

### Megoldás

```
isTree(adj : E1*[n]) : L
  FOR i := 1 to n-1
    color[i] := W
  l := true
  count := 0
  color[0] := G
  q := Queue()
  q.add(0)
  AMÍG l és !q.isEmpty()
    u := q.rem()
    color[u] := B
    count := count + 1
    p := adj[u]
    AMÍG l és p != NULL
      HA color[p->key] = G
        l := false
      KÜLÖNBEN
        HA color[p->key] = W
          color[p->key] := G
          q.add(p->key)
      p := p->next
  return l és count = n
```

* Mikor fa a fa? Ha összefüggő és körmentes
* Akkor összefüggő, ha a végén minden csúcs fekete lett, azaz minden csúcs előbb bekerült, később kikerült a sorból. Ha ekkor növelünk egy számlálót, amit a végén összevetünk n-nel, akkor ezt a kérdést meg tudjuk válaszolni
    * A count helyett lehetett volna egy ciklus is, ahol vizsgáljuk, maradt-e fehér csúcs, de így hatékonyabb (bár nem nagyságrendileg)
* A körmentesség pedig akkor biztosított, ha minden csúcsba csak egy módon juthatunk el
    * Ha egy csúcsból fehér kimenő él van: az a csúcs még nem volt, most felfedeztük, bevettük a sorba
    * Ha egy csúcsból fekete kimenő él van: a célcsúcs fekete, azaz le van zárva már. Következésképpen már az aktuális csúcs előtt néztük. De mivel irányítatlan a gráf, ez az él annak a csúcsnak is kimenő éle volt, tehát ahol most vagyunk, akkor vettük be a sorba. Ezért ez egy olyan él, amire már rábólintottunk. Ha nem így lett volna, akkor az a csúcs, amiből most kiindulunk, akkor szürke lett volna, amikor ezt az élt elsőre vizsgáltuk. De akkor pedig a harmadik ágra mentünk volna és kiléptünk volna
    * Ha egy csúcsból szürke csúcsba megy a kimenő él: ez esetben ezt a csúcsot már máshonnan is elértük, és mivel végső soron mind egy csúcsból leszármazott csúcsok, ezért ez csalhatatlan jele annak, hogy kör van

## W12 (2020.12.02.) 1. feladata

* Legyen éllistával adott egy irányítatlan, élsúlyozott gráf a világ összes matematikusaival
    * Két matematikus között akkor van él, ha van olyan publikáció, aminek ők társszerzői
    * Az élsúly a közös kontribúciók számát jelenti
    * A gráf csúcscímkéi a matematikusok nevei
* Írj algoritmust, ami a leghatékonyabb módon visszaadja E. W. Dijkstra Erdős-számát
* Feltehetjük, hogy mindkét érintett személy szerepel a gráfban
* Az éllista tömbje most nem számokkal, hanem stringekkel (nevekkel) van indexelve
* Hasonlóan, az elemek kulcsai is lehetnek stringek
* Dijkstrát a struktogramban rövidíthetjük „EWD”-nak, Erdőst pedig „EP”-nek
* Definíció. Erdős Pál Erdős-száma 0. Egy tudós Erdős-száma n, ha az általa írt cikkek társszerzői között a legkisebb Erdős-szám n-1
* Pluszpontért: tippeld meg, konkrétan mennyi E. W. Dijkstra Erdős-száma

### Megoldás

* Elsőre ijesztő feladat lehet, de miről is van szó?
    * Van egy gráf, amiben akkor van összekötve két csúcs, ha köztük direkt kapcsolat (is) van; akkor érhető el egy csúcs egy másikból k hosszú élsorozattal, ha k köztük a távolság
    * A szélességi bejárás úgy működik, ha k távolságon el lehet jutni A-ból B-be, de k-1 távolságon már nem, akkor, ha A-ból indítjuk, B.d-hez ezt hozzá fogja tudni rendelni. Ez pedig épp az Erdős-szám definíciója
    * Tehát nekünk szélességi bejárást kell indítanunk EP-ből vagy esetleg EWD-ből kiindulva, s amint megtaláljuk a másikat, returnölni kell a friss d értékkel
    * Egy-két dolog nem is érdekel minket: az élsúlyozottság, a publikáló társak listája (azaz a pi által adott útvonal); a színek is igazából csak annyira, hogy működjön az algoritmus; és a többi matematikus Erdős-száma se hoz lázba
    * Ezek alapján a BFS kis átalakításokkal lesz az ideális választás:

```
getErdosNumberOfDijkstra(adj : E1*[String]) : N U végtelen
  FOR EACH m eleme adj.getKeys()
    color[m] := W
    d[m] := végtelen
  color["EP"] := G
  d["EP"] := 0
  q := Queue()
  q.add("EP")
  AMÍG !q.isEmpty()
    u := q.rem()
    color[u] := B
    p := adj[u]
    AMÍG p != NULL
      HA color[p->key] = W
        HA p->key = "EWD"
          return d[u] + 1
        KÜLÖNBEN
          color[p->key] := G
          d[p->key] := d[u] + 1
          q.add(p->key)
      p := p->next
  return végtelen
```

* Mivel tudjuk, hogy "EP" és "EWD" két külön csúcs, ezért most biztosan nem lesz a startcsúcs (d-je) a megoldás. Ha a feladat ezt az esetet megengedné, akkor már u-ra kéne vizsgálni, hogy az "EWD"-e, és annak d-jével kéne visszatérni, nem pedig a v-k bejárásakor kéne ezt nézni
* A végtelen érték valid, ha nem érhető el egymásból a két csúcs (irányítatlan, tehát ez a viszony szimmetrikus). Csak azt tettük fel, léteznek, azt nem, elérhetők-e
* 0 egyébként nem lesz a visszatérési érték
* Az algoritmusban a "fehér csúcs" esetet kellett csak kiegészíteni, itt gátlástalanul returnölünk, amint megtaláltuk Dijkstrát, nyilván ezt máshogy is lehet strukturálni, ez így teljesen jó
* A pluszpontos feladat elsőre kicsit random lehet..., de két dolgot teszteltem vele:
    * Egyrészt, értitek-e, hogy szemléletesen ez mit jelent (pl. abból, hogy nem írtok olyat megoldásnak, hogy 0, stb). Az Erdős-szám nem sok mindenkit érint, de az az elv, amit használunk, a távolság, mint fogalom, igazából mindenkinek a mindennapjai része
    * Másrészt, hogy megérzitek-e, hogy egy nagy és sűrű gráfban ez a szám nem lehet túl nagy. És innen kezdve, már nem is annyira nehéz telibetrafálni
    * Amúgy a megoldás 4
* Ez amúgy egy ZH-feladat volt nálam
