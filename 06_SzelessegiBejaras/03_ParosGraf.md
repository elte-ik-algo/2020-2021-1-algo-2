# Gráfok

## Szélességi bejárás

## Páros gráf

* Szélességi bejárás alapalgoritmusára épített algoritmussal döntsük el egy éllistával adott irányítatlan gráfról, hogy páros-e
* Mit is jelent a páros gráf?
	* Van olyan V1, V2 részhalmaza V-nek, hogy V1 ∪ V2 = V, de V1 ∩ V2 = ∅, valamint minden (u,v) ∈ E élre igaz, hogy ha u ∈ V1, akkor v ∈ V2, és ha u ∈ V2, akkor v ∈ V1
	* Magyarul: fel lehet bontani két olyan diszjunkt részhalmazra (lehet olyan osztályozást csinálni), hogy a részhalmazokon belül egy él se megy, hanem minden él a két részhalmaz között
* Ötlet: reformáljuk meg a szélességi bejárás színezését. Legyen három szín: fehér, piros, kék. Kezdetben mindenki fehér, kivéve a kezdőcsúcs, ami mondjuk piros. Legyen az egyik halmaz a pirosak halmaza, a másik a kékek halmaza. Ekkor nyilván ha feldolgozok egy piros csúcsot, az összes gyereke kék kell, hogy legyen, és vice versa. Ha fehér csúccsal találkozok, csak beszínezem. Ha ellentétes színűvel, akkor elengedem, és ha azonos színűvel, akkor hibát jelzek
* A felesleges részeket pedig nyugodtan elhagyhatjuk

```
isEven(adj : E1*[n]) : L
    FOR i = 0 to n-1
        color[i] := W
    q := Queue()
    color[0] := R
    q.add(0)
    isEven := true
    AMÍG isEven és !q.isEmpty()
        u := q.rem()
        p := adj[u]
        AMÍG isEven és p != NULL
            HA color[p->key] = W
                color[p->key] := getReverse(color[u])
                q.add(p->key)
            KÜLÖNBEN
                HA color[p->key] = color[u]
                    isEven := false
            p := p->next
    return isEven
```

```
getReverse(color : Color) : Color
    HA color = B
        return R
    KÜLÖNBEN
        return B
```

* isEven tulajdonképpen egy optimista lineáris keresés l változója, a szélességi bejárás felsorolóra építve
* Természetesen akár a változót el is hagyhattuk volna és ki is léphettünk volna ott, ahol már biztosan kiderül, hogy isEven nem lehet igaz
* Ha van izolált csúcs, vagy eleve, ha nem összefüggő a gráf, akkor hasonlóan, ahogy az általános algoritmusnál említettük, újra el kell indulni a még fehér csúcsokkal
    * Az mindegy, egy ilyen fehéret pirosra vagy kékre kezdünk el színezni
* Az megoldás-e, ha eleve minden csúcsot a sorba rakunk?
    * Az nem, mert lehet, hogy másodiknak nem az első egy szomszédját vennénk ki, hanem pl. egy kettő távolságra levőt tőle. És ha ezt pont ellentétesre színeznénk, akkor amikor összeér a két csúcs, ellentmondásra jutunk, pedig nem kéne. Az egész szélességi bejárásnak a lényege a folyton kiterjesztett sor
