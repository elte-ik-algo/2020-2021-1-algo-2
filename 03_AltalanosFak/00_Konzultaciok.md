# Konzultációs feladatok általános fa témában

## W09 (2020.11.11.) 1. feladata

* Írjunk algoritmust, ami kiírja egy általános fa teljesen zárójelezett alakját, mégpedig hierarchikus zárójelekkel: {}, [], ()
* Ötlet: egy paramétert beteszünk még az algoritmusba, mely azt adja meg, milyen szinten járunk a fában, és a szinttől függően választjuk a megfelelő zárójelt
* Példa:

```
    2
   / \
  1   3
 /|\
5 4 6
 /  | \
9   7  0
|
8
```

* Output:

```
{2[1(5)(4{9[8]})(6{7}{0})][3]}
```

### Megoldás

* Az alapja a hivatalos jegyzetben található, valóban kiegészítettük egy plusz paraméterrel

```
printWithParentheses(t : Node*)
  printWithParentheses(t, '{')
```

* Klasszikus wrappelt függvény, olyan segédváltozóhoz, ami a rekurzió fenntartásához kell, de amúgy kívülről nem érdekes
* Itt a gyökérhez tartozó zárójeltípust adtuk át
* Akár egy számot is átadhattunk volna, ami szintenként növelődik és modulo vesszük az adott zárójelt

```
printWithParentheses(t : Node*, opening : Ch)
  HA t != NULL
    print(opening)
    print(t->key)
    printWithParentheses(t->child1, nextLevel(opening))
    print(closing(opening))
    printWithParentheses(t->sibling, opening)
```

* Tehát kiírjuk a nyitót, aztán magát a kulcsot, majd a gyerekeket a következő zárójel szinten, végül a megfelelő szintű csukót és utána ugrunk a testvérre, ami velünk azonos szinten van

```
closing(opening : Ch)
  HA opening = '('
    return ')'
  HA opening = '['
    return ']'
  HA opening = '{'
    return '}'
```

```
nextLevel(opening : Ch)
  HA opening = '('
    return '{'
  HA opening = '['
    return '('
  HA opening = '{'
    return '['
```

* Nem kell KÜLÖNBEN HA, vagy VHA-kat írni, mert minden ágon returnölünk, úgyis egy és csak egy fut le

## W09 (2020.11.11.) 2. feladata

* TODO

## W09 (2020.11.11.) 3. feladata

* Adott egy két pointerrel, láncoltan ábrázolt általános fa. Egy csúcsban az első gyerekre és a testvérre mutató pointerek vannak
* Készítse el a következő algoritmust: megadja annak a szülőnek a címét, amelynek a legtöbb gyereke van. Adja meg azt is, hogy hány gyereke van
* Felteheti, hogy a fa nem üres, elegendő egy ilyen szülőt megadni, ha a maximum nem egyértelmű

### Megoldás

* Az alternatív reprezentációs fejezetben hasonló algoritmust közlök

```
maxChildren(t : Node*) : (Node* × N)
  c := 0
  max := t
  maxChildren(t, c, max)
  return (max, c)
```

* (max, c) párral térek vissza, max egy Node*, c egy N, azaz a pár egy eleme a Node* × N direktszorzatnak (ami egy halmaz)
* Felteszem, hogy a gyökér a max, 0 gyerekkel

```
maxChildren(t : Node*, &c : N, &max : Node*)
  HA t->child1 != NULL
    children := countSiblings(t->child1, c, max)
    HA c < children
      max := t
      c := children
```

* c-t és maxot referencia szerint adjuk át, mert ezekbe írhatunk
* t-ről itt feltesszük, hogy nem NULL - az alapesetben ezt a feladat kiírása szerint tudjuk, a rekurzív szinteken pedig ellenőrizzük mindig hívás előtt
* Ha van gyerek, megszámoljuk, mennyi. A countSiblings számolja, hogy az első gyerek és a többi hányan vannak testvérek - a paraméterként, referencia szerint kapott változókba kerülnek ezek az értékek
* Itt ha max marad a t, az is OK, a c akkor is frissül

```
countSiblings(t : Node*, &c : N, &max : Node*) : N
  siblings := 0
  AMÍG t != NULL
    siblings := siblings + 1
    maxChildren(t->child1, c, max)
    t := t->sibling
  return siblings
```

* Itt úgy vesszük, t egy nem NULL csúcs, amit a testvéreivel össze kell számolni
* Kiindulunk tehát t-ből, és mint egy láncolt listát, bejárjuk a siblingeket és számolunk
* Amikor egy csúcsot nézünk (magát t-t is beleértve), mindig meghívjuk a rekurziót ezek gyerekeire is
* Ennek eredményeképp, amikor returnölünk a siblings változóval, a hívás helyén c és max már ennek a gyereknek a leszármazottai alapján frissítve van

* Az órán nézett példa:

```
    2
   / \
  1   3
 /|\
5 4 6
 /  | \
9   7  0
|
8
```

## W10 (2020.11.18.) 1. feladata

* Adott egy két pointerrel, láncoltan ábrázolt általános fa. Egy csúcsban az első gyerekre és a testvérre mutató pointerek vannak
* Készítse el a következő kereső algoritmust: megadunk egy kulcsot, ha a fában van ilyen kulcs, akkor kiírja a csúcsig vezető úton a szülők kulcsait. Elképzelhető, hogy az adott csúcs több helyen is szerepel a fában, akkor mindegyik előforduláshoz írja ki az útvonalat! Az utat a gyökértől indulva, a csúcsig kell kiírni
* A feladat pl. a DOS ```dir fájlnév /s``` parancsával személtethető: egy alkönyvtár rendszerben megkeressük azokat az alkönyvtárakat, amelyekben az adott fájlnév előfordul, kiírjuk a fájlhoz vezető elérési útvonalat

### Megoldás - ha VAN ```parent``` pointer

```
printPaths(t : Node*, key : T)
  AMÍG t != NULL
    printPaths(t->child1, key)
    HA t->key = key
      printPath(t->parent)
    t := t->sibling
```

```
printPath(t : Node*)
  HA t != NULL
    printPath(t->parent)
    print(t->key)
```

* Postorder bejárással veszünk minden csúcsot
    * Igazából a sorrend nem számít, csak hogy minden csúcs be legyen járva
* A rekurziónál átadjuk a key paramétert is
* A "process()" most az, hogy ha a keresett kulccsal megegyezik az adott elem kulcsa, írjuk ki az ősöket (az adott elemet ne)
* Ezután továbbmegyünk, mert több kulcsra is matchelhet a megadott paraméter
* Az ősök kiírása szintén rekurzív, itt t már az első kiírandó elem, tehát az illeszkedő csúcs szülője. Ez nem biztos, hogy létezik, ezt ellenőrizzük, majd előbb meghívjuk ugyanezt az algoritmust ennek a szülőjére (aminek léte szintén ellenőrizendő) és utána írjuk csak ki őt - ezáltal biztosítjuk, hogy a gyökértől lefelé írjuk ki az elemeket
* Műveletigénye annyiszor az adott ág hossza (max. a fa magassága), ahányszor illeszkedés van (max. a csúcsok száma)

### Megoldás - ha NINCS ```parent``` pointer

```
printPaths(t : Node*, key : T)
  q : Queue<T>
  printPaths(t, key, q)
```

* Mivel nincs szülő pointer, egy sorba mentjük a gyökértől az adott csúcsig tartó utat, ha ki kell írni, ennek tartalmát kell kiírni

```
printPaths(t : Node*, key : T, q : Queue<T>)
  AMÍG t != NULL
    q.add(t->key)
    printPaths(t->child1, key, q)
    printAndRemoveLast(q, t->key = key)
    t := t->sibling
```

* Ez is egy postorder-szerű bejáráson alapszik, bár a sorba a gyerekek feldolgozása előtt rakom be az adott csúcsot. A sorban a gyökértől a csúcsig kell lennie a csúcsoknak ebben a sorrendben, emiatt előbb kell a szülőt berakni, mint a gyereket
* A rekurzív hívás után, tehát a ```printAndRemoveLast()``` csinálja az érdemi kiírást, és annak az elemnek a kivételét, amit két sorral (és persze az összes leszármazott végigjárásával) ezelőtt beraktunk. Amikor a testvérre megyünk, annak a "gyökértől vezető útján" nem szerepel az előző testvér, amikor pedig a testvéreket bejárjuk és visszatérünk a szülőhöz, annak a leszármazási láncán szintén nem szerepel annak az utolsó gyereke. Ezért tehát ezt a kivételt mindenképp meg kell tenni

```
printAndRemoveLast(q : Queue, shouldPrint : L)
  q.add('#')
  AMÍG q.first() != '#'
    first := q.rem()
    HA q.first() != '#'
      q.add(first)
      print(first)
  q.rem()
```

* Kapunk egy feltételt, hogy "ki kéne-e írni" az utat (ez akkor igaz, ha az adott csúcs key-e a paraméterként megadott kulcs)
* Sort egyesével tudunk végigjárni és (legalábbis a tárgy során tanult verzióban) ilyenkor muszáj is kivenni az elemeket. Persze akár tömböt is használhatnánk, de mi flancolunk, Dominika
* Berakunk egy "extremális elemet" - a '#' helyébe képzeljünk bármi olyat, ami nem lehet valós kulcsérték
* Amíg ezt nem érjuk el, addig kivesszük a sor legrégibb elemét és ha most már a # a legrégibb, akkor ez az eredeti sor utolsó eleme volt, tehát elhagyjuk (a feladat az, hogy az adott csúcsot a végére dobjuk ki, ill. ezt ne is írjuk ki, csak az őseit)
* Ha pedig nem az utolsó elemről volt szó, akkor vissza is rakjuk és ki is írjuk
* A végén még kidobjuk a "könyvjelzőnek" használt #-et
* Egy példa gráf és az output:

```
  a
/ | \
b c  d
     |
     b
     |
     c
     |
     b
```

```
a
adbc
ad
```

## W10 (2020.11.18.) 2. feladata

* Írjunk algoritmust, ami kiszámolja a paraméterként megadott hagyományos (azaz "bináris láncolt") módon ábrázolt általános fa magasságát

### Megoldás - Elegáns, tisztán rekurzív

* Mivel ebben az ábrázolásban az általános fa felfogható egyfajta elforgatott bináris faként, használhatjuk az ott megtanult algoritmust...
* ... azzal a megkötéssel, hogy a siblingnél nem adunk hozzá egyet, hiszen az ebben a fában nem számít mélyebb szintnek

```
h(t : Node*) : Z
  HA t = NULL
    return -1
  KÜLÖNBEN
    return max(h(t->child1)+1, h(t->sibling))
```

```
max(a : Z, b : Z) : Z
  HA a > b
    return a
  KÜLÖNBEN
    return b
```

* A magasság típusa Z, hiszen részeredményként, és üres fa esetén végeredményként is lehet -1
* Gyors keresztkérdés: megoldás-e, ha megszámoljuk a nem null "child1"-eket?
    * Ezzel az a gond, hogy ha egy ágon eljutottunk mondjuk 3 child1-ig, de ennek a testvérében is van 3 child1, az csak 3 mélység, mégis 6-nak számoljuk
    * Ha annyiban módosítunk az elképzelésen, hogy csak "minden ágon" számolunk, akkor kb. meg is kaptuk az alább következő algoritmust, ui. az a saját rekurziós szintjének height változóját -1-re inicializálja, és csak akkor nő vissza ez 0-ra legalább, ha a p nem null, a p pedig kezdetben a child1

### Megoldás - Vegyes

* Ha a fa üres, akkor -1 a magasság
* Ha nem, akkor mivel maga a fa a gyökerével van megadva, annak biztosan nincsen testvére, legfeljebb csak gyereke
* Ebből az elgondolásból kiindulva azt fogjuk tenni, hogy egy p pointert rámutattatunk a t (első) gyerekére, ennek kiszámoljuk a magasságát (úgy értve, hogy ő egy fa gyökere, akinek a testvéreivel nem kell foglalkoznunk), majd továbblépünk ennek testvérére, annak is kiszámoljuk, stb.
* A magasság kiszámolása egy rekurzív függvényhívás
* A kiszámolt magasságokkal egy maximumkiválasztást végzünk
* Végül az eredményhez hozzáadunk 1-et, az reprezentálja magát t-t
* A rekurzióban egy csúcsot mindig egy részfa gyökereként adunk át, tehát a testvéreket nem nézzük, azt csak a külső ciklusban

```
h(t : Node*) : Z
  HA t = NULL
    return -1
  KÜLÖNBEN
    height := -1
    p := t->child1
    AMÍG p != NULL
      h := h(p)
      HA h > height
        height := h
      p := p->sibling
    return height+1
```

## W12 (2020.12.02.) 2. feladata

* Legyen n>2 természetes szám
* Igaz-e, hogy meg lehet adni minden ilyen n-re olyan n csúcsszámú általános fát, hogy annak mind a preorder, mind a postorder, mind a szintfolytonos bejárására épített, a látogatott kulcsokat sorban kiíró algoritmus outputja ugyanaz legyen?
* A fában nem lehet minden csúcs kulcsa ugyanaz az érték
* Válaszod indokold

### Megoldás

* A trükk az, hogy a feltétel, miszerint "nem lehet minden kulcs ugyanaz" nem zárja ki, hogy néhány kulcs viszont mégis azonos legyen. Ha a fa egy lánc, akkor a preorder bejárás "felülről lefele", a postorder "alulról felfele" járja be ezt a láncot, a sorfolytonos pedig ilyenkor a preorderrel megegyezik. Tehát csak az kell, hogy a lánc szimmetrikus legyen
* Tehát n csúcsú fára, ha n páros, legyen a gyökér kulcsa 1, a bal gyerekéé 2, annak a bal gyerekéé 3, és így tovább n/2-ig. Utána ugyanígy felveszünk n/2 csúcsot, az előző csúcs bal gyerekeként, a címkéket csökkentve, egészen 1-ig
* Páratlan esetben még beszúrunk középre egy tetszőleges számot
* Csak n=3-tól kell működnie
	* Ott ha a középső elem nem azonos a két szélsővel, akkor teljesül a feltétel
	* n=2-re nem tudnánk ilyet mondani, de nem is kértük
* Példa n=4-re és 5-re:

```
      1
     /
    2
   /
  2
 /
1
```

```
        1
       /
      2
     /
    3
   /
  2
 /
1
```

* Persze más megoldás is lehet, de a lényege a szimmetria behozása a pre-post fordított sorrendisége miatt
* Fun fact: minden bináris fa egyben általános fa is (ezért általánosítás az általános fa), tehát ha ez a feladat bináris fákra lett volna megfogalmazva, akkor is helyes lenne ugyanez a megoldás - még ha reprezentációs szinten máshogy is nézne ki

# W12 (2020.12.02.)-be be nem férő feladat

* Írj algoritmust, ami minimális műveletigénnyel kiszámolja a paraméterként megadott általános fa kulcsainak átlagát
* A kulcsok természetes számok lehetnek
* A fa „bináris láncolt” ábrázolással van megadva, és biztosan helyes
* Üres fa esetén az algoritmus -1-gyel térjen vissza

## Megoldás

* A feladat tehát az, hogy járjuk be a fát valahogy (én most preorder módon fogom, de ennek nincs jelentősége), s gyűjtsük ki egyrészt a kulcsok értékeinek összegét, másrészt a csúcsok számát
* Ha ez megvan, ezt a két számot el kell osztani egymással
* Persze üres fa esetén a 0-val osztást kezelni kell
* Illetve jelölni kell, hogy bár a kulcsok természetes számok, az eredmény az osztás miatt (nemnegatív) racionális szám lesz
* Fontos, hogy ne járjuk be kétszer a fát a két adat miatt, mert az felesleges
* A megoldásaitokat (mert ez egy éles zh feladat volt - a szerk.) elnézve gyakori hiba volt, hogy véletlen nem minden csúcsot jártatok be, de az is előfordult (valakinél a kettő egyszerre), hogy egy csúcs valahogyan többször számolódott. A rekurzióval könnyű elcsúszni. Ha kódismétlés van az algoritmusban az sok szempontból nem szerencsés, de egyfajta vészjelzőként is szolgálhat: általában azok számoltak egy csúcsot kétszer, akiknél kódismétlés volt. Érdemes ezt ellenőrizni
* Bár máshogy is megoldható, de én a klasszikus rekurzív függvényt választottam, ahol egy külső függvény (aminek a szignatúrája az elvárt) inicializál, egy belső, rekurzív függvény pedig a referencia szerint átadott segédváltozóba gyűjtöget
* Még gyakori hibák: a null-ellenőrzés elmaradása; olyan változók lekérése, amik nincsenek is (pl. gyerekek listája); a szignatúra elhagyása

```
average(t : Node*) : Q
  HA t = NULL
    return -1
  KÜLÖNBEN
    sum := 0
    count := 0
    computeSumAndCount(t, sum, count)
    return sum/count
```

```
computeSumAndCount(t : Node*, s& : N, c& : N)
  HA t != NULL
    s := s + t->key
    c := c + 1
    computeSumAndCount(t->child1, s, c)
    computeSumAndCount(t->sibling, s, c)
```

* Kiválóan látszik a megoldásban, hogy előbb "processzálom" a t->keyt, majd meghívom a gyerekre, s végül a testvérre ugyanazt (ami végülis azt jelenti, hogy előbb a node, majd a gyerekei balról jobbra - azaz preorder bejárás szerint)
* Elsőre a belső nullcheck feleslegesnek tűnhet, de a rekurzív hívásoknál szükséges lesz. Azt is csinálhattam volna, hogy a külső struktogramban nincs nullcheck, de a végén ha a count 0 maradt, -1-gyel térek vissza. A ternáris operátorral ez egy egész rövid kódot adna (de ezt csak kódban, itt papíron nem használom)

# W12 (2020.12.02.)-be be nem férő feladat

* Adj struktogramot „bináris láncolt” ábrázolású általános fa egy olyan bejárására, aminek a műveletigénye lineáris, de van olyan input, amire eltérő sorrendben sorolja fel a csúcsokat, mint a tanult nevezetes bejárások bármelyike

## Megoldás

* Egy kis élelmességgel az előző feladat megoldásával ezt össze is lehetett volna vonni - azt hiszem, ezt senki sem csinálta, pedig sok időt nyerhettetek volna vele (ez volt a következő feladat az ominózus zh-n - a korr.)
* Mit is csinál a 3 tanult algoritmus?
    * Preorder: előbb én, majd a gyerekeim balról jobbra (azaz egyfajta mélységi bejárás)
    * Postorder: előbb a gyerekeim balról jobbra, majd én (egyfajta "fordított" mélységi bejárás)
    * Szintfolytonos: balról jobbra a szintek szerint
* Nyilván, nincs olyan bejárás, ami minden inputra különböző lenne (üres fára mindegyik üres)
* De ha sok gyerek van, akkor ezt a "balról jobbra"-dolgot talán el lehetne rontani, és akkor találhatunk ilyet
    * A random sorrend elég megfoghatatlan ezzel az ábrázolással, ilyen műveletigénnyel
    * De a rekurzió lehetőséget ad a fordított (jobbról balra) megoldásra, erre mutatok kettő megoldást is

```
traversal(t : Node*)
  HA t != NULL
    traversal(t->sibling)
    process(t)
    traversal(t->child1)
```

```
traversal(t : Node*)
  HA t != NULL
    process(t)
    traversal(t->sibling)
    traversal(t->child1)
```

* Az előadáson vett ciklusos-rekurziós megoldások nem jelentenek jó alapot ennek az algoritmusnak, mert ott mindenképp a "továbblépés" kell legyen az utolsó parancs, különben kimaradnának bizonyos node-ok
