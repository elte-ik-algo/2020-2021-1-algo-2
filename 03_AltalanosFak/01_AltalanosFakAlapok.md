# Általános fák

## Szemlélet

* A korábban bevezetett bináris fa fogalmát általánosítjuk úgy, hogy a fa minden csúcsának immár bármennyi (akár 0, akár 1) gyereke lehet
* Ebből az is következik, hogy nincsen egy dedikált "bal" és "jobb" gyerek, tehát nincs bal és jobb részfa se
* Ha van pl. 3 gyerek, akkor ők és a leszármazottaik definiálnak 3 részfát, de nincs olyan, hogy ha nincs egy gyerek se, akkor a bal (vagy jobb) részfa üres helyét üres részfaként értelmeznénk (nincs olyan, hogy "üres részfa")
* Természetesen üres fa ugyanúgy létezik, mint eddig
* Nincs felső korlát a gyerekek számára, de egy csúcsnak se lehet "végtelen sok" gyereke
* A nem üres fáknak ugyanúgy, mint eddig, most is van egy egyértelmű gyökércsúcsa
* Egy csúcs gyerekei között ugyan itt most már nem lesz "bal" és "jobb", de mégis lehet valamiféle egyértelmű sorrendiséget mondani köztük, amit itt vizuálisan úgy jelölünk, hogy a gyerekeket balról jobbra soroljuk fel. Az más kérdés, hogy nem minden alkalmazásban fontos ez a sorrendiség
* Példa:

```
    a
   / \
  b   j
 /|\
c d g
 /  | \
e   h  i
|
f
```

* Ennek a fának "a" a gyökere
* "c", "f", "h", "i" és "j" a levelei
* "a"-nak és "g"-nek kettő, "b"-nek 3, "d"-nek és "e"-nek csak egy közvetlen gyereke van; a leveleknek értelemszerűen 0
* A fa magassága 4 - hasonló elven számolható, mint a bináris fák esetében
* "b" gyerekei közül "c" az első, "d" a második és "g" az utolsó

## Reprezentáció

* A félév során alapértelmezésben az ún. "bináris láncolt" reprezentációt alkalmazzuk
* A csúcsok típusát úgy, mint a bináris fák esetében, Node-nak fogjuk hívni (és a Node3 is ugyanúgy megvan a szülő pointerrel, mint tavaly), az adattagok és a konstruktorok tulajdonképpen átnevezésen túl azonosak a bináris fáknál tanultakkal:

```
Node<T>
- child1 : Node*
- sibling : Node*
- key : T

+ Node()
    child1 := NULL
    sibling := NULL

+ Node(x : T)
    child1 := NULL
    sibling := NULL
    key := x
```

* Persze itt más a jelentés:
    * A "child1" jelentse a gyerekek közül azt, amelyiket vizuálisan a leginkább baloldalra rajzolnánk. Akkor és csak akkor NULL, ha nincs gyerek egyáltalán
    * A "sibling" pedig a közvetlen jobb oldali szomszéd gyereket (persze csak azok közül, akiknek azonos a szülője). Akkor és csak akkor NULL, ha az adott csúcs "legjobboldalibb" gyerek
* Ez alapján a fenti fa a reprezentációhoz közelibb szemléltetéssel így néz ki:

```
    a
   /
  b---j
 /
c-d-g
 /  |
e   h--i
|
f
```

* Ha a lefelé mutató vonalak a "child1" pointereket, a vízszintes vonalak a "sibling" pointereket jelölik
* Érdemes tudatosítani, hogy az "e"-ből nem megy él a "h"-ba, hiszen azok nem testvérek (hanem "unokatestvérek")
* A gyerekek közti "sorrendet" úgy lehet megfogni, hogy egy adott csúcs child1 pointerének értékét egy a csúcs gyerekeit tartalmazó egyszerű láncolt listaként fogjuk fel, aminek a next pointere a sibling
* Láthatjuk, hogy a fa kvázi megfelel egy elfordított bináris fának - ezért mindent, amit bináris fával lehet csinálni, ezzel a fajta fával is, bár a jelentése nyilván más lehet