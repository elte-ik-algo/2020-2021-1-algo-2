* [Algo2 2020-2021/1. 03/01 - Általános fák - bevezetés](https://youtu.be/jPRxKd3r1y8)
* [Algo2 2020-2021/1. 03/02 - Általános fák nevezetes bejárásai](https://youtu.be/v8yJl8tmH7E)
* [Algo2 2020-2021/1. 03/03 - Általános fák parse-olása szöveges alakból](https://youtu.be/oo3DUH6IRHk)
* [Algo2 2020-2021/1. 03/04 - Általános fa levélszám algoritmus](https://youtu.be/8Dw2s0osvug)
* [Algo2 2020-2021/1. 03/05 - Általános fa alternatív reprezentációval](https://youtu.be/XcbG2uqOD0k)



