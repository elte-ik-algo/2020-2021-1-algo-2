# Pontozási rendszer

## Nagyobb dolgok

### Zárthelyi

* Két alkalom
    * Sőt, technikailag több "első" alkalmat is kiírok, lásd Teamsen
* Van pótalkalom mindkettőhöz, rontani nem lehet
* Ha így se elég, lesz egy utolsó, azaz kb. "gyakuv", ami felülírja a két zh pont összegét, a többi lehetséges pontszámra nem hat, rontani nem lehet
* Első zh témakörei: szélességi bejárásig bezárólag minden
* Második zh témakörei: mélységi bejárástól kezdve minden
* Formátum:
    * Két feladat
    * Egy kódírós, egy lejátszós
    * Mindkettő más témában
    * Mindkettőhöz egy vagy több minikérdés
    * Szövegesen kell beadni, gépelve canvason vagy képben Teamsen
* Egy zh 40 pontos
* Nincs minimumpont elvárás

### Szóbeli

* Két alkalom egyedileg megbeszélt időpontban
* Első + második zh témája
* Kb. 10 perc / alkalom, de ahogy alakul
* 3 véletlenszámot generálok, egyik se lehet azonos
    * Mindegyik egy-egy témát jelent
    * Ebből az egyiket kiválaszthatod, a kiválasztottból nem kérdezek
    * A téma egy viszonylag tág dolog, ebből fogok egy (vagy ha úgy alakul) több konkrét kérdést feltenni, amire elvárás, hogy közel azonnal válaszolj
* Mindegyik témára 10 pont jár maximum
* Azaz egy szóbelire 10*2 = 20p
* A két szóbelire összesen 40p
* Nincs minimumpont elvárás
* Mindkét szóbeli egyszer javítható

### Megajánlott vizsga ötös

* Megajánlott vizsga 5-öst ér, ha a szóbelikből és írásbelikből összesen elértél összesen 96 pontot (80%-ot)

### Vizsga feltétele

* A megcsúszásaim miatt nincs
* Bárki mehet vizsgázni, de kérlek vegyétek figyelembe, ha a félév végéig nem lesz meg a legalább 2-es gyakjegy, a vizsgát is utólag törölhetik

## Bónuszpontok

### Első és második minta zh

* 2×60 pontos zh
    * Legalább 30 ponttól 1 bónuszpont
    * Legalább 40 ponttól 2 bónuszpont
    * Legalább 50 ponttól 3 bónuszpont

### Canvasos kvízek

* 10 ponttól: 2p
* 7 ponttól: 1p

### Kahootos kvízek

* 10 darab
* Részvételért: 1p
* Top5-beli helyezésért: +1p

### Kis szorgalmik Canvasban

* 24 darab, 22 db 1 pontos, 2 db 2 pontos
* Javítható
* Összesen elérhető: 26p

### 2 kódolós szorgalmi Canvasban

* Első és második zh témájából 1-1
* 5 féléből lehet választani mindkét témakörből, egy írható meg
* Javítható
* 2×5p

### Órai munka

* Eseti jellegel 1-1p

### Hibabejelentés

* Gitlab, canvas, bárhol
* Kis hibák: minden 5. után 1p
* Nagy hibák: 1p
* Ha gitlab -> MR (ne forkolj!)

### Sakk

* Aki nyert kahootot, megpróbálhatja
* Ha megver: 1p

# Pontszámítás alapja

* Ketteshez "elvárt":
    * Összes kahooton részt venni (10p)
    * Két 50%-os zh (40p)
    * Szóbelin a minimum (16p)
    * Összesen: 66p
* Maximum reálisan elérhető:
    * Összes kahooton részt venni, kb 3-on benne lenni a top 10-ben (13p)
    * Két 80%-os zh (64p)
    * Fele kisszorgalmi (13p)
    * 2 kódolós szorgalmi (10p)
    * Hibátlan szóbeli (40p)
    * Plusz még lehetne számolni az órai munkát, sakkot, minta zh-kat, stb., tehát ez se igazi maximum, de ezzel számolok
    * Összesen: 140p

# Ponthatárok

* Elégséges (2): 66 - 79
* Közepes (3): 80 - 105
* Jó (4): 106 - 124
* Jeles (5): 125 - 140 and beyond

# Szóbeli "tételsor"

* Első szóbeli
    1. Tömörítésről általában és naiv módszer
    2. Huffman-kód
    3. LZW-kód
    4. Tömörítési algoritmusok összehasonlítása
    5. AVL-fa koncepció, keresőfa-tulajdonságok, kiegyensúlyozottság
    6. AVL-fa: forgatások beszúráskor
    7. AVL-fa: forgatások törléskor
    8. B+-fa koncepció, fokszám, tulajdonságok, invariánsok
    9. B+-fa műveletek
    10. Általános fa bináris láncolt reprezentáció, bejárások, algoritmusok
    11. Általános fa alternatív reprezentáció, bejárások, algoritmusok
    12. Gráfok típusai, reprezentációi, mit mikor érdemes
    13. Egyszerűbb gráfalgoritmusok
    14. Szélességi bejárás feladata, algoritmusa, műveletigénye, lejátszása, implementációi, rekonstrukciója
* Második szóbeli
    1. Mélységi bejárás feladata, algoritmusa, műveletigénye, lejátszása, felhasználása körkeresésre, élek osztályzása
    2. Topologikus rendezés befokokkal és mélységi bejárással
    3. Erősen összefüggő komponensek - mit jelent, hogy kell kiszámolni
    4. Félig összefüggőség eldöntése - mit jelent, hogy kell kiszámolni
    5. MST: általános algoritmus (piros-kék nem kell)
    6. MST: Kruskal és Prim
    7. Dijkstra-algoritmus feladata, megkötései, implementációs javaslatai, lejátszása
    8. Bellman--Ford-algoritmus feladata, megkötései, lehetséges implementációi, lejátszása
    9. Floyd--Warshall-algoritmus feladata, lejátszása, különbsége a Warshall-algoritmushoz képest
    10. Mintaillesztés feladata, naiv algoritmusa
    11. KMP-algoritmus
    12. Quick Search mintaillesztő algoritmus
