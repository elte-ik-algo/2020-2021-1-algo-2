# Hibák az anyagokban

* Minden megtalált hibáért pont jár!
* Amit tudok, javítok is a helyén, a többiről itt található egy lista

# Gráfok

* A [gráf reprezentációval kapcsolatos diasor](../05_GrafAlapok/SajatVideosAnyag/05_02_Grafok_Reprezentacio.pptx)ban több dián is, és a hozzá tartozó [videóanyagban](https://www.youtube.com/watch?v=qQWl73R2Ctw) 18:51-től tévesen jelölöm a bal alsó gráf élistájában a C->A élt 1 súlyúnak, az helyesen 0 lenne az ábra alapján. A 0, ellentétben a végtelennel élsúlyozott gráf éllistájában egy helyes érték és valóban, egy 0 súlyú élt jelent, ami nem mindenhol életszerű, de formailag abszolút lehetséges

# Mintaillesztés

* A KMP-vel kapcsolatos magyarázó [videó](https://www.youtube.com/watch?v=YzAN9smPteI) 5. percének környékén említem, hogy next[0] = 0 mindig, és ezt meg lehet jegyezni, mint alapszabály. Ne tegyétek, mert a next 1-től van indexelve, a next[1] = 0 állítás viszont valóban igaz
