# Veszteségmentes adattömörítés

* A példákban stringeket - karakterek sorozatait - fogunk tömöríteni, ami végül is kellően általános eset, hiszen minden más adatszerkezet is felírható valamilyen jelek sorozataként
* Miért beszélünk "veszteségmentesről"?
    * Mert a cél az, hogy reprodukálható legyen az eredeti kód
* És miért "tömörítésről"?
    * Mert a cél az, hogy kisebb legyen a fájlméret
* Ez a két feltétel egymás ellen dolgozik, nem könnyű mindkettőt jó arányban teljesíteni
* Alapból úgy vesszük, egy karakter egy bájton (B), azaz 8 biten (b) van tárolva

## Gyakoriságtábla

* A Naiv módszerhez és a Huffman-kódhoz vegyük az alábbi példát
* Tegyük fel, hogy a tömörítendő string 100 karakteres. Az egyszerűség kedvéért ezt most nem adjuk meg, viszont elemein végigmenve felírtuk egy asszociatív tömbbe (dictionary, map), hogy az egyes karakterek külön-külön hányszor szerepeltek a stringben. Ezt hívjuk gyakoriságtáblának:
    * A: 5
    * B: 13
    * C: 17
    * D: 7
    * E: 50
    * F: 8
* Ha a feladatunk része a gyakoriságtábla elkészítése, akkor a végén érdemes ellenőrzési céllal összeadni a megkapott számokat, valóban kijön-e az eredeti string (itt 100) hossza

## Egyszerű – naiv – tömörítés

* Karakterenkénti tömörítés, fix kódhosszal
    * Ez azt jelenti, hogy azt csináljuk, hogy minden egyes karakternek egyértelműen megfeleltetünk egy bitsorozatot, úgy, hogy ezek a bitsorozatok azonos hosszúak
* A példában összesen 6 féle karakter van, azaz 6 féle különböző jellel már egyértelműen kódolhatók
    * Annyi biten kódolunk minden betűt, amennyiből már kijön a 6 különböző sorozat, azaz ahány biten legalább 6 különböző jelet tudunk kódolni
    * Minden bit 0 és 1 lehet, azaz kétféle, egymástól függetlenül, azaz egy x bites sorozat összesen 2<sup>x</sup> különböző értéket kódolhat
    * Esetünkben 2<sup>3</sup> = 8 >= 6, ezért 3 biten kódolunk
    * Pl.: A: 000, B: 001, C: 010, stb.
* A teljes szöveg hossza eredetileg 100 * 1B, azaz 800b volt
* Most, tömörítve 100 * 3 b, azaz 300 biten elfér
* A dekódoláshoz persze mellékelni kell az eredeti szöveg ábécéjét is, illetve vagy a kódolási megfeleltetéseket, vagy annak stratégiáját, ahogy azok kijöttek