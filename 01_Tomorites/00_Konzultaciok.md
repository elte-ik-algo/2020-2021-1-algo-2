# 4. hét - 2020. 09. 30. - Konzultáció veszteségmentes adattömörítés témában

* Feladatmegoldások Nagy Ádám anyaga alapján
* Az óra videófelvétele [itt](https://youtu.be/IYDxnSvPWAE) található

## Alapok és naiv tömörítés


* Input:
```
s = AABCAADEAAB
```

### 1. Naiv módszert alkalmazva hány bittel tudjuk ábrázolni az s-t?

* 5 féle karakter van, amit 3 biten tárolhatunk, mert 2^3 = 8, ami nem kisebb, mint 5
* 11 betű van: 11*3 = 33 bit lesz a kódhossz
* Példa megfeleltetés:

```
A   000
B   001
C   010
D   100
E   101
```

### 2. Adjuk meg s egy kódolt alakját naiv módszer esetén

* Szóban megadtuk :)

### 3. Adjunk meg betűnkénti kódolást, amely tömörebb eredményt ad, mint a naiv módszer!

* Betűnkénti: a lényeg, hogy megint egy betűhöz egy kódot rendelünk, és konzekvensen egy bizonyos betűhöz mindig ugyanazt a kódot
* Arról viszont nincs szó, hogy a kódoknak egyforma hosszúnak kell lenniük
* Ugyanakkor, arra vigyázni kell, hogy egyértelműen dekódolható legyen az eredmény, tehát pl. legyen prefixmentes a kód
* Pl.:

```
A   0
B   100
C   101
D   110
E   111
```

* Itt az A-ból 6 db van, ezért 6 bit
* A többi 3 biten van és összesen 5 db van belőlük, ezért azok 15 bitet foglalnak
* Összesen 21 bit

### 4. Adjunk meg egy egyenletes kódolást, amely tömörebb eredményt ad, mint a naiv módszer!

* Egyszerű, de nagyszerű ötlet, ha ugyanazt csináljuk, mint az 1. feladatnál, de ne az "A", hanem az "AA" legyen a "000"
* Hiszen az összes A párban van ebben a konkrét inputban, tehát az A-kat így feleannyi biten tároljuk, de a többi karakteren sem bukunk
* Persze attól még, hogy erre a konkrét inputra optimalizálunk, azért más, az adott ábécé feletti kóddal is jó ha elbírunk, ezért a sima A-hoz is rendeljünk valamit
* Az "egyenletesség" azt jelenti, hogy a kódszavak hossza azonos, ezért legyen pl. ez az 111
* Ekkor a dekódolás egyértelmű (mivel azonosak a kódhosszak, nincs többértelműség), és az enkódolás se problémás, A esetében legyen az a szabály, ha párosával van, a 000-t használjuk, ha páratlanul, akkor a maradékra az 111-et
* Fontos látni, ez nem naiv és nem is valamelyik másik nevezetes kódolás, hanem tetszőleges

### 5. Rajzoljuk fel a korábban használt kód kódfáját! Milyen tulajdonsággal rendelkezik egy egyenletes kódhoz tartozó kódfa?

* Azaz ennek:

```
A   111
AA  000
B   001
C   010
D   100
E   101
```

* Ez nem egy Huffman-kódfa, csak egy random fa, nem is kell, hogy bináris legyen
* Az élcímkézés se kötelezően olyan, mint a Huffmannál, ezért itt most ténylegesen rá is írjuk, hogy tudjuk
* A gyökér az üres kódszó, ebből kiindulva olvasunk prefixeket (épp, mint a Huffmannál), és így érünk el a kódszavakat reprezentáló csúcsokig - amik nem szükségképpen a levelek

```

                 ""
                /  \
               /    \
             0/      \1
             /        \
          "0"          "1"
          / \          / \
        0/   \1      1/   \0
        /     \      /     \
     "00"    "01"  "11"    "10"
     /  \      |     |      /  \
   0/    \1    |0    |1   0/    \1
"000"  "001" "010" "111"  "100" "101"
 (AA)   (B)    (C)  (A)    (D)   (E)
```

* Egyenletes kód kódfájának tulajdonságai:
    * Minden levél ugyanazon a szinten van (mégse nevezném teljes fának, mert maradhatnak ki ágak, nem is feltétlen bináris)
    * Minden kódszót reprezentáló csúcs egyben levél is

## Huffman-kódolás

### 1. A mintához hasonlóan végezzük el a Huffman-kódolást egy általunk (általam) választott tetszőleges szöveg esetén!

* A változatosság gyönyörködtet:

```
AABCAADEAAB
```

* Gyakoriságtábla:

```
A   6
B   2
C   1
D   1
E   1
```

* Ellenőrizzük, 11 a sorok összege, és valóban, ennyi karakter van

```
   C1    D1
    \   /
     \ /
E1    2
 \   /
  \ /
   3  B2
    \/
A6  5
 \  /
  11
```

* Érdekes, amikor az E-t dolgoztuk fel (2. lépés) azt akár a B-vel is vehettük volna, nem kellett volna feltétlen a C-D közös csúccsal. A Huffmannál nincs kőbe vésve, melyiket kell választani, elvileg azonos kódolási aránynak kell kijönnie minden esetben
* Kódok:

```
A: 0
B: 11
C: 1010
D: 1011
E: 100
```

* Eredmény:

```
A: 1 * 6
B: 2 * 2
C: 4 * 1
D: 4 * 1
E: 3 * 1
```

* Azaz: 21 bit + fa
* Ugyanúgy 21 bit, mint az előző rész 3. feladatánál, kb. az lett volna az eredmény, ha a nemdeterminisztikus résznél a másik párral vesszük fel az E-t

### 2. Keressünk olyan szöveget, ahol a Huffman-kódolás pontosan annyi bittel történik mint a naiv módszerrel!

* Példák: A, AB, ABCDEFGH, AAABBBCCCDDD
* Arra jutottunk, hogy minden karakternek azonos létszámban kell jelen lennie, illetve 2 hatványnak kell lennie a teljes hossznak, hogy a Huffman-fa egy teljes fa legyen

### 3. Írjunk programot (struktogramot), amely a bemenetként kapott kódolt szöveg és hozzá tartozó Huffman-kód kódfája alapján helyreállítja az eredeti szöveget!

* Kihagytuk, mert kódolós szorgalmi lesz (lásd a szorgalmi kiírások között)

### 4. Írjuk meg a Huffman-kódolást megvalósító programot. Bemenet egy szöveg, kimenet a kódolt szöveg és a kódfa!

* Kihagytuk, mert kódolós szorgalmi lesz (lásd a szorgalmi kiírások között)

### 5. Számoljuk ki a ternáris (r = 3) Huffman-kódot a korábbi példára!

* Kihagytuk, mert kisszorgalmi lesz (lásd a szorgalmi kiírások között)

### 6. Verseny a csoportban: Keressük azt a 10 hosszú szöveget, amelyet a legnagyobb mértékben lehetett tömöríteni a minimális szóhosszt használó naiv módszerhez képest (Huffman-kóddal)

* Három pályamű érkezett, bár a legutolsó lapzárta után

#### Első versenyző

```
ABCDEABCDE
```

* Kódfa:

```
A   B C   D
 \ /   \ /
  4     4   E
   \     \ /
    \     6
     \   /
      \ /
       10
```
* Kódhosszak:

```
A 2*2
B 2*2
C 3*2
D 3*2
E 2*2
```

* 30 vs 24, plusz a fa
* Nem az igazi, mert bár nem 2 hatvány az ábécé hossza, tehát nem spórol a naiv módszer, de mindenből ugyanannyi van, így nem jön ki a Huffman haszna

#### 2. versenyző

```
AAAAAAAABC
```

* Itt a naiv 20 bites, hiszen 10 karakter van, de a C miatt már két bitet igényel
* A Huffman pedig 12, mert az A-nak egy hosszú ágat tudunk csinálni (8*1 bit), a B-nek és C-nek pedig 2 hosszút, ami így összesen 8 + 2 + 2 bit
* Plusz persze a fa
* Itt érezzük, ha több A lenne, ez mégjobb lenne

#### 3. versenyző

```
ABCDEEEEEE
```

* Hasonló, mint az előző, itt is épp, hogy már kell a következő bit, de épp emiatt a Huffmannak van lehetősége egy biten is tárolni, amit jól esik neki
* Tehát 5 féle karakter, ami miatt 3 bit kell már
* 10 hosszú a string, ezért naivul 30 bit kell
* Huffman-fa:

```
A  B  C   D
 \ /   \ /
  2     2
   \   /
    \ /
     4   E
      \ /
       10
```

* Értékek:

```
Kód Hossz Db  Össz
A   3     1   3
B   3     1   3
C   3     1   3
D   3     1   3
E   1     6   6
SUM           18 bit
```

* Érdekes, ez pont ugyanaz az arány, mint az előbb, de itt is hasonlóan, az E-k számának növelésével a végtelenségig lehet nyitni az ollót a két módszer között

# A W12-ből (2020.12.02.) kimaradó két ide vonatkozó feladat

## Első feladat

* Szemléltessük a Huffman- és az LZW-kódolást a **PERKELEPERKELTPERELT** stringen
* Eredetileg egy byte-os kódolást feltételezve ez a 20 betűs string 20 B helyet foglal
* Mondjuk meg - vezessük le - hány bitre sikerül tömöríteni a Naiv megoldással, illetve a Huffman- és LZW-kódolásokkal

### Megoldás

#### Naiv (egyszerű) kódolás
* A különböző karakterek: P, E, R, K, L, T; összesen 6 féle
* Minden karakterhez következetesen hozzárendelünk egy egyedi kódot
* 2<sup>x</sup> >= 6, x=3, emiatt az egyedi kódok hossza minden esetben 3
* Pl.: P - 000, E - 001, R - 010 stb.
* Összesen 20 betű van, mindegyike 3 bites, összesen 60 bit (+ az ábécé)

#### Huffman-kódolás
* Készítsük el a gyakoriságtáblát (utolsó oszlop csak ellenőrzés, hogy számoltunk-e minden elemet):
    
```
P E R K L T Össz
3 7 3 2 3 2 20
```

* Vegyük mindig a két legkisebb gyakoriságot, építsünk belőle Node-okat, vonjuk össze, címkézzük meg az új Node-ot a gyakoriságösszeggel
* Amikor már készülnek a fák, a táblázatbeli elemek mellett a fák gyökerei is ugyanúgy "játszanak" a minimumok keresésekor
* Lehet, hogy mindkét minimum a táblázatból jön, lehet a fából, de lehet vegyesen is

```
K2 T2
 \/
 4
```

```
K2 T2 P3 R3
 \/     \/
 4      6
```

```
  K2 T2 P3 R3
   \/     \/
L3 4      6
 \/
  7
```

* Eddig elég egyértelmű, innen viszont két megoldás is lehet, mindkettőt végigcsinálom:

```
  K2 T2   P3 R3
   \/       \/
L3 4    E7  6
 \/       \/
  7       13
```

```
  K2 T2   P3 R3
   \/       \/
L3 4    E7  6
 \/       \/
  7       13
   \     /
    \   /
     \ /
     20
```

* Innen a gyökértől kezdve az összes ágon a "balra megyek 0, jobbra megyek 1" protokollal bejárva az alábbi kódokat kapom:
    * P - 110
    * E - 10
    * R - 111
    * K - 010
    * L - 00
    * T - 011
* Gyakoriságot szorozva a kódhosszal megkapjuk, hogy az adott karakter összes előfordulása kódolva mennyi helyet foglal:
    * P - 3 * 3 = 9
    * E - 7 * 2 = 14
    * R - 3 * 3 = 9
    * K - 2 * 3 = 6
    * L - 3 * 2 = 6
    * T - 2 * 3 = 6
* Összesen: 50 bit (+ kódfa)
* Ez így teljesen korrekt és jó megoldás. De van még egy, ami nem csak címkézésben, hanem a fa alakjában is más. Mi lesz akkor a kódolási hatékonyság?

```
  K2 T2 P3 R3
   \/     \/
L3 4      6
 \/      /
  7     /
   \   /
    \ /
     13
```

```
  K2 T2 P3 R3
   \/     \/
L3 4      6
 \/      /
  7     /
   \   /
    \ /
E7   13
  \ /
  20
```

* Kódok:
    * P - 110
    * E - 0
    * R - 111
    * K - 1010
    * L - 100
    * T - 1011
* Bitek:
    * P - 3 * 3 = 9
    * E - 7 * 1 = 7
    * R - 3 * 3 = 9
    * K - 2 * 4 = 8
    * L - 3 * 3 = 9
    * T - 2 * 4 = 8
* Összesen: 50 bit (+ kódfa)
* Tehát amíg a szabályokat betartjuk, rugalmas, merre megyünk. Sikerült tömöríteni a naivhoz képest

#### LZW-kódolás

* Az input: PERKELEPERKELTPERELT
* A kezdeti szótár:

```
1 P
2 E
3 R
4 K
5 L
6 T
```

* Lépésről lépésre:

```
in = P|ERKELEPERKELTPERELT
1 P
2 E
3 R
4 K
5 L
6 T
7 PE
out = <1>
```

```
in = PE|RKELEPERKELTPERELT
1 P
2 E
3 R
4 K
5 L
6 T
7 PE
8 ER
out = <1,2>
```

```
in = PER|KELEPERKELTPERELT
1 P
2 E
3 R
4 K
5 L
6 T
7 PE
8 ER
9 RK
out = <1,2,3>
```

```
in = PERK|ELEPERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
out = <1,2,3,4>
```

```
in = PERKE|LEPERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
out = <1,2,3,4,2>
```

```
in = PERKEL|EPERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
out = <1,2,3,4,2,5>
```

```
in = PERKELE|PERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
out = <1,2,3,4,2,5,2>
```

```
in = PERKELEPE|RKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
out = <1,2,3,4,2,5,2,7>
```

```
in = PERKELEPERK|ELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
out = <1,2,3,4,2,5,2,7,9>
```

```
in = PERKELEPERKEL|TPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
out = <1,2,3,4,2,5,2,7,9,11>
```

```
in = PERKELEPERKELT|PERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
17 TP
out = <1,2,3,4,2,5,2,7,9,11,6>
```


```
in = PERKELEPERKELTPER|ELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
17 TP
18 PERE
out = <1,2,3,4,2,5,2,7,9,11,6,14>
```

```
in = PERKELEPERKELTPERELT|
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
17 TP
18 PERE
out = <1,2,3,4,2,5,2,7,9,11,6,14,16>
```

* Az output 13 karakteres, egy 18 elemű ábécén, azaz minden egyes számot x biten kódolunk, ahol 2<sup>x</sup> >= 18
* x = 5, tehát 13 * 5 = 65 bit a kód hossza, plusz az ábécé
* Ez nem egy jó tömörítés erre a példára

## Második feladat

* Írjuk meg az LZW dekódolás algoritmusát

### Megoldás

```
lzwDecode(in : InStream) : OutStream
    out := <>
    stringTable := initFromAlphabet()
    c := in.read()
    current := stringTable.get(c)
    out.print(current)
    c := in.read()
    AMÍG c != EOF
        HA c eleme stringTable
            next := stringTable.get(c)
        KÜLÖNBEN
            next := current + current₁
        out.print(next)
        stringTable.add(current + next₁)
        current := next
        c := in.read()
    return out
```

* Az inputunk most a kód, a konzolra pedig az eredeti stringet szeretnénk kiírni
* az initFromAlphabet()-et már ismerjük, úgy általában a kódolást akarjuk "visszafelé" megvalósítani
* a stringTable.get() itt most kódhoz rendel stringet, nem stringhez kódot, az add() ugyanazt csinálja, mint a kódolós algoritmusnál
* current₁ jelentse a current szó első karakterét
* Feltételezzük, hogy a kód helyes és nem üres
* Az elején kiolvassuk az első kódot. Ez biztosan az ábécé egyik eleme, hiszen kezdetben ezek voltak csak a szótárban, ezeket tudtuk kódolni
* Amikor elkezdődik a ciklus, az alábbiakat tudjuk:
    * Ki van írva az első betű
    * c-ben a második betű (vagy itt már lehet akár részstring is) kódja
    * currentben az első betű
* Általában, c-ben az utolsó feldolgozott kód utáni kód van, azaz, ha ez EOF, kiléphetünk
* Ha nem EOF, és benn van a szótárban, akkor kérjük le, hogy minek a kódja ő, ezt írjuk is ki (hiszen ebből az inputból lett ez a kód) - ez a next változó, majd gondoljuk el azt, hogy amikor még az előző kódot írtuk ki (ne feledjük, a ciklus előtt azt már dekódoltuk!), akkor még adósak vagyunk azzal, hogy az annál eggyel hosszabb stringet elkódoljuk. Ezt megtesszük most, a currentben volt az előző kódolandó string, a nextben a mostani, az előzőt felvesszük a next első karakterével megtoldva, majd mivel mindent megtettünk, tovább is léphetünk. Ha épp az utolsó karakternél vagyunk, azt kiírjuk, majd a továbblépésnél kilépünk a ciklusból, azaz az utolsó karakter által kódolt szó + a következő által kódolt szó első karakterét nem fogjuk már kiírni a szótárba, hiszen ilyen nincs is
* Érdekesebb eset a másik ág. Feltételezzük, hogy az input (a teljes kód) helyes. Tehát ha az a kódindex, ami most még nincs a string táblában egy helyes kódolt részstring volt mégiscsak, akkor feltételezhetjük, hogy amikor a kódolást csináltuk, már ismert volt a string táblában ez a kód. Hogy lehet ez? A kódolás minden lépése után felveszünk egy elemet. Az első lépés után tehát |ábécé|+1, a k. lépés után |ábécé|+k a szótár elemszáma. Megjegyzem, az utolsó után nem veszünk fel, tehát végül |ábécé|+|lépések|-1 lesz a méret
    * De ezt a kódolásnál látott működést hogy reprodukáljuk a dekódolásnál? Láthattuk az előző ágon: amikor kiírjuk a k. elem kódját, akkor (mivel csak mostanra tudtuk meg a teljes értékét) vesszük fel az k-1. elemnél eggyel hosszabb stringhez tartozó kódot, azaz az |ábécé|+k-1-edik kódot. Azaz amikor elágazásunk ehhez a sorhoz ér a k. karakternél, a string tábla mérete még csak |ábécé|+k-2, hiszen a stringTable.add() még csak később jön
    * Kódoláskor a k. lépésnél keletkezik az |ábécé|+k. indexű bejegyzés, de kiírni maximum csak az |ábécé|+k-1-es indexű kódot fogjuk (hiszen előbb volt a kiírás és utána a felvétel)
    * Dekódoláskor mivel eggyel el vagyunk csúszva - helyes inputot feltételezve -, ez az egyetlen olyan kódindex, amivel úgy találkozhatunk, hogy még nem ismerjük (pedig kódoláskor ismertük). Nagyobb egyszerűen nem lehet, a kisebbeknél pedig a másik ágra futunk
    * Viszont ha ez az |ábécé|+k-1. kód, akkor a k-1. lépésben született. Ebben a lépésben a k-1. substringet írtuk ki kódolva és a k. elem első betűjét konkatenálva a végére kódoltuk el és vettük fel, mint |ábécé|+k-1 kódú elem
    * Tehát tudjuk azt, hogy dekódoláskor a k. lépésben velünk inputként szembejövő |ábécé|+k-1. kódindex a k-1. lépés dekódolt stringjénél "eggyel nagyobb". A k-1. lépés dekódolt stringje pedig a current változó értéke
    * Ehhez a current változóhoz csapjuk most hozzá ezt a stringet..., amiről tudjuk, hogy a current változóval kezdődik. Egész pontosan nem más, mint a current változó, plusz a következő string első betűje, na de a következő string első betűje önmaga, ha nem így lenne, akkor a másik ágon lennénk. Emiatt mondható, hogy a next nem lesz más, mint a current + a current első betűje
* Az órai példában ilyenkor volt az, hogy leírtam a megfelelő betűk helyére "ismeretleneket" (x,y,z), amiket később visszahelyettesítettem. Volt olyan eset, amikor az utolsó két karakter is ismeretlen volt. Ebből az utolsó igazából csak a pufferelés miatti még nem beolvasott nextbeli első karakter, amit ennek az algoritmusnak az előreolvasó mivolta meg is old, a másik ilyen ismeretlen karakter pedig mindig azon esetben jön elő, amikor a dekódolandó kód épphogy az előbbi lépésben lett felvezetve a táblába. Ezért ilyenkor nem is kell az ismeretlenekkel számolgatni, hanem az algoritmus alapján kimondható, hogy az új kód utolsó karaktere egyben az előző kód első karaktere. Ezt az igazságot akár a lejátszásnál is alkalmazhatjuk mostantól
