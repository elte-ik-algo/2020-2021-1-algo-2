# 2. zh infó

## Időpont(ok), formátum, pontozás

* Maximum 3 időpontot hirdetek meg, amiknek a pontos idejét Teamsen, szavazással döntjük el
* Opcionálisan, szükség esetén lesz még egy 4. alkalom is, de az csak pótzh-ra lesz nyitva
* A zh Canvason lesz elérhető, mint kvíz egy fél órás idősávban
* Mindenki összesen két alkalommal írhatja meg, azaz kétszer nyithatja meg Canvasban az adott kvízt. Többszöri megnyitásra érvénytelenítem
* Két feladat lesz
    * Az egyik biztosan gráfos a lenti tematikából
    * A másik biztosan mintaillesztéses a lenti tematikából
    * A mintaillesztéses biztosan lejátszós lesz + kisebb elméleti kérdést tehetek fel
    * A gráfos lehet lejátszós és algoritmusírós is
* Az összpontszám 40, ami 25-15 arányban fog megoszlani a gráfos és a mintaillesztős feladat között
* Kötelezően elérendő minimum nincs

## Tematika

* Gráfok
    * Mélységi bejárás (DFS) algoritmusa, műveletigény, lejátszása. Alkalmazásai (élek osztályozása, körfigyelés (DAG-tulajdonság), topologikus rendezés (befokokkal is), erősen összefüggő komponensek, félig öszefüggőség
    * Minimális költségű feszítőfák (MST) elmélete, általános algoritmusa, két tanult konkrét algoritmusa (Prim, Kruskal) implementációkkal, műveletigénnyel, lejátszással
    * Legrövidebb utak egy csúcsból. Dijkstra-algoritmus, legrövidebb utak DAG-ra, Bellman-Ford. Mindre lejátszás, algoritmus, műveletigény
    * Legrövidebb utak minden csúcspárra. Elmélet, Floyd-Warshall, Warshall. Algoritmus, műveletigény, lejátszás
* Mintaillesztés
    * Minden algoritmusnál lejátszással, műveletigénnyel
    * Elmélet, Brute Force
    * KMP
    * QS

## Gyakorlási lehetőség

* Anyagok átnézése, benne gyakorló feladatokkal
* "Központi kvízek" 7-8-9-10. feladatsora 
* "Kisszorgalmik" 23-24. feladata
* "Kódolós szorgalmik" 2. adagból a 2-3-4-5. feladatok
* "2. ZH" nevű gyakorló kvíz, melynek egyik megoldása hamarosan itt fenn lesz
