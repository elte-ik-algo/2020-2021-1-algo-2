# Második minta zh

* A [videó](https://youtu.be/VjzQQbKpfnY)
* Amire érdemes figyelni - és ez a vizsgára szól
    * Egyesével kapjátok a kérdéseket és nincs visszalépés
    * A végén lehet nehezebb, hosszabb kérdés, tartalékoljatok (az én mintám nem releváns, mert beszéltem közben, így sokkal lassabb)
    * Ha jól láttam, automatikusan NEM adta be a munkám amikor lejárt az idő, mindig kattintsatok konkrétan a beadás gombra (bár korábban mintha ezzel ellentétes működést tapasztaltam volna)
* Az utolsó feladatot nem tudtam ellenőrizni, de a többi alapján egy hiba lett:
    * A DAG-os legrövidebb utas feladatban az "út hossza" kérdésre 4-et adtam meg, de nem a fizikai hosszára (amit az "e" adna meg az egyik algoritmusban) volt kíváncsi a kérdés szerzője, hanem az összköltségére, ami 7. Ez volt a helyes válasz
    * Amúgy 10 pontos a kérdés, és 8,57-et adott, ami pontosan a 6/7 része 10-nek (7 kérdés volt). Így lehet indirekt ellenőrizni, hogy 1 hibás kérdés volt

# Első feladat (DFS)

* Részletek a videóban: 00:01:53 - 00:17:45
* Érdemes rajzolni, nálam ez a kép készült:

![Első feladat - DFS](algo2-2020_2021_1-zh2_minta_1.png)

# Második feladat (MST - Prim algoritmus)

* Videóban: 00:17:45 - 00:33:35
* Ábra:

![Második feladat - MST](algo2-2020_2021_1-zh2_minta_2.png)

# Harmadik feladat (Legrövidebb utak egy csúcsból - DAG-ban)

* Videóban: 00:33:35 - 00:51:55
* Ábra:

![Harmadik feladat - Legrövidebb utak DAG](algo2-2020_2021_1-zh2_minta_3.png)

# Negyedik feladat (Mintaillesztés - KMP)

* Videóban: 00:51:55 - 01:16:00
* Jegyzeteim a megoldáshoz:

```
P = ABACABA
T = BABACABACABABACABACA
szigma = {A,B,C}

i =       1 2 3 4 5 6 7
          A B A C A B A
next[i] = 0 0 1 0 1 2 3

                  1 1 1 1 1 1 1 1 1 1 2
1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0
B A B A C A B A C A B A B A C A B A C A
A B A C A B A                      
×                                  
  A B A C A B A                    
  - - - - - - -                    
          A B A C A B A            
                - - - -            
                  A B A C A B A    
                        ×          
                      A B A C A B A
                        - - - - - -
                              A B A C A B A
                                    - -

S={1,5,11}
```

```
12345678901234567890
1234567           
×                 
        1234567  
        ×        
                1234567
```

# Ötödik feladat (Bellman-Ford struktogram kiegésztítése)

* Videóban: 01:16:00 - 01:23:35
* Erről nem készült plusz jegyzet

# Hatodik feladat (Dijkstra-szerű algoritmusírás)

* Videóban: 01:23:35 - 01:54:55
* 18 percem maradt rá, mint látható, ez nem jött össze, duma nélkül, szűken talán-talán igen, de mindenképp érdemes erre a feladatra legalább 25 percet szánni, ugyanakkor azt se szabad elfelejteni, hogy ez is 10-et ér, meg az előző (jóval könnyebb, illetve könnyebben tanulható) is

* Feladat szövege:
    * A T/1 : bit[m,n] mátrix egy téglalap alakú teret reprezentál, ahol 0 jelöli az éjszaka kivilágított, 1 pedig a sötét helyeket. A világos területek éjszaka is biztonságosak, a sötéteken azonban tanácsosabb nem járni. Írjon fejléces, megfelelően paraméterezett struktogramot, amely megadja, hogy legalább hány sötét ponton, azaz 1 értékű mátrix elemen kell átmennünk a tér bal felső sarkából a jobb alsó sarkába úgy, hogy minden pozícióról a 4 oldalszomszédjára léphetünk, átlósan pedig nem léphetünk! (Érdemes deklarálni a D : int[m,n] tömböt, aminek (i,j) koordinátái egy irányított gráf csúcsai, D[i,j] elemei a csúcsok d-értékei, a (0;1), (0;-1), (-1;0), (1;0) lépésirányok pedig a gráf élei, feltéve, hogy nem lépünk ki a mátrixból. A gráf élsúlyozott, ui. minden olyan éle, amelyik sötét pontba visz, 1 súlyú, minden olyan éle pedig, amelyik világos helyre visz, 0 súlyú. Így az algoritmusunk legrövidebb út keresése lesz egy élsúlyozott gráfon.) Műveletigény: O((m*n)^2).
* Kód:

```
howManyUnsafe(T/1 : bit[m,n]) : N
  neighbours := { (0;1), (0;-1), (-1;0), (1;0) }
  D : int[m,n]
  topLeft := (1,1)
  bottomRight := (m,n)
  minQ : MinQ
  for i := 1..m
    for j := 1..n
      HA i=1 és j=1
        D[i,j] := 0
      KÜLÖNBEN
        D[i,j] := végtelen
        minQ.add(i,j, végtelen)
  u := topLeft (u : [1..m]×[1..n])
  AMÍG D[u] < végtelen és !minQ.isEmpty()
    FOR i := 0 to |neighbours|-1
      neighbour := u + neighbours[i]
      HA neighbour_1 > 0 és neighbour_1 <= m és u + neighbour > 0 és neighbour_2 <= n
        weight := D[u] + T[neighbour]
        HA weight < D[neighbour]
          D[neighbour] := weight
          minQ.adjust(neighbour)
    u := minQ.remMin()
    HA u = bottomRight
      return D[bottomRight]
return végtelen
```
* Példa:

```
m=4
n=5

? 0 0 0 0
1 1 1 1 0
1 1 1 1 0
1 1 1 1 1

0 0 0 0 0
1 1 1 1 0
2 2 2 2 0
3 3 3 3 0
```

* Ebben volt néhány hiba:
    * Abban az ifben, ahol eldönti, nem indexel-e ki a szomszédos mező a mátrixból, hiányzik az alsó index az egyik "neighbour" mögül
    * A "return végtelen" egy bekezdéssel beljebb kéne legyen, mert így a szignatúrával van egy szinten
    * A struktogram alatti ábrán a bottomRighthoz tartozó 0 biztos nem jó, ha T-ben 1-es volt, akkor D-ben is legalább 1-es. Esetünkben pontosan 1-es
* Illetve még egy fontos komment: Ordó((m×n)^2) volt az elvárt műveletigény. Tehát ennyi, vagy kevesebb. Ezt nem fejtettem ki
    * Itt a csúcsok száma m×n
    * Az élek száma pedig az alábbi módon jön ki: egy sorban van n elem. Mindenki a szomszédokkal van összekötve, tehát ez n-1 db él. Minden sorban ez van, ez így m×(n-1). Ezen kívül ugyanez igaz az oszlopokra is, tehát van még n×(m-1) él is. Ez összesen: (mn-m) + (nm-n) = 2mn - m - n, azaz a csúcsok számának (mn) lineáris függvénye
    * Magyarul a gráf ritkának számít. Ilyenkor a minQ jó ha kupac, a gráf reprezentációja itt se nem éllistás, és meglepő módon nem is mátrixos, akkor se, ha amúgy egy mátrix az input. Miért? Mert az éleken nem egy dupla for ciklussal megyünk végig..., mármint nem a csúcsok száma szerinti dupla for ciklussal (azaz n×m×n×m-es ciklussal!)
    * Az inicializáció Théta(csúcsok száma) = Théta(m×n)
    * Egy kivételével minden csúcs bekerül a sorba és biztos mindegyik ki is kerül - igazából a D[u] < végtelen feltétel ide nem is kell most, mivel összefüggő a gráf - tehát az algoritmus érdemi része is kap egy m×n-es szorzót
    * Itt minden csúcshoz fixen 4 élt nézünk meg, ennyiszer hívódhat meg az adjust() is
    * Mint láttuk, összesen az élek száma kb. annyi, mint 2-szer a csúcsok száma, ezt kb. duplán nézzük az irányítatlanság miatt, de akárhogyanis a külső ciklus, aminél kivesszük a minQ-ból az elemeket, és a belső ciklus, amikor feldolgozzuk egy adott csúcs szomszédait, összesen nagyságrendileg a csúcsok számaszor fut le. Összesen! Azaz marad az m×n-es szorzó az adjust()-hoz és a remMin()-hez
    * Ezek mindketten log(csúcsok száma) = log(m×n)-esek
    * Ebből összesen az jön ki, hogy a második része m×n×log(m×n), ráadásul mégis Ordó, mivel elképzelhető, hogy a bottomRightot nem épp m×n. alkalomra járom be, s amint bejárom, kilépek a ciklusból. Leghamarabb ez m+n-nél megtörténhet
    * Összesen tehát Théta(m×n) + Ordó(m×n×log(m×n)) a műveletigény, ami bőven megfelel az elvártnak
    * Ez alapján el tudom képzelni, hogy nem ezt a megoldást "várta" a feladat kiötlője, de szerintem nincs benne semmi hiba. Ha mégis, aki meggyőz, kap egy pontot :)
