# Info

* Határidő: lehetőleg minél hamarabb, de leginkább amíg akarsz gyakjegyet kapni -1 nap
* A megoldást a canvas felületen adjátok le (ha valamiért nem megy, Teamsen küldjétek)
* A beadás formája:
    * Algoritmusírás esetén pszeudokód vagy struktogram
    * Osztályok esetén osztálydiagram + algoritmusok (lásd fent)
    * Lejátszós feladat esetén az órai stílushoz hasonló lépésről lépésre illusztrált lejátszás
    * Valamint az összesre vonatkozóan: írhattok hozzá mindenféle szöveges magyarázatot, ha a feladat ezt kéri, vagy Ti szükségesnek érzitek
    * Programnyelvi kódot itt nem tudok elfogadni - de fogok kiírni célzottan kódolós feladatokat is
* Hacsak más nincs mondva, minden feladat 1 pontot ér
* Hibás megoldásokat lehet javítva újraküldeni egy ideig, erről a konkrét visszajelzésben írok

# 01 - Veszteségmentes adattömörítés

## 01_HuffmanFreqTable

* Készítsd el a Huffman-kódolás gyakoriságtáblájának megfelelő FreqTable típust. Adj reprezentációt, valamint implementációt a következő műveleteknek:
	* addFreq(Char) - A paraméterként megadott karakter számosságát növeli a táblában. Ha még nem volt ilyen karakter, 1-re inicializálja a számosságot
	* getFreq(Char) - visszaadja a megadott karakter gyakoriságát (0 is lehet)
	* decFreq(Char) - Csökkenti eggyel a megadott karakter gyakoriságát (0 esetében marad 0)
	* remFreq(Char) - Nullázza a megadott karakter gyakoriságát
	* remMinFreq() : (Char, N) - Kiveszi a legkisebb gyakoriságú elemet a táblából, visszatér annak nevével és számosságával egy pár formájában. Előfeltétel: ne legyen üres a tábla
	* remMaxFreq() : (Char, N) - Kiveszi a legnagyobb gyakoriságú elemet a táblából, visszatér annak nevével és számosságával egy pár formájában. Előfeltétel: ne legyen üres a tábla
	* size() : N - Megadja, hány féle karaktert tárolunk a táblában
	* totalFreqs() : N - Megadja a reprezentált string hosszát
* Legyen adott egy MAX_SIZE : N konstans. Feltehetjük, hogy nem lesz ennél több elemű ábécével dolgunk
* Bármilyen előző félévben tanult reprezentációt használhatsz,  műveleteket egészen elemi szintig fejtsd ki
* Törekedj a műveletigények minimalizására rendeltetésszerű (Huffman-kódoláshoz való) használat esetén
* Röviden indokold meg, miért ezt a reprezentációt választottad, hasonlítsd össze, más szóba jöhető reprezentációkkal
* Érték: 2p

## 02_HuffmanTree

* Írj algoritmust, ami egy (akár üres) InStream formában megadott kódolandó string paraméterből előállít egy annak megfelelő Huffman-kódfát
* A fa legyen az előző félévben tanult láncolt reprezentációjú bináris fa - azaz a Node típust használjuk
* A csúcsok címkéje (kulcsa) most legyen egy pár: a betű, amit kódol (nem-levél csúcsok esetén üres karakter), és az adott csúcs által lefedett gyakoriság
* Élcímkék ne legyenek
* Tegyük fel, hogy adott a FreqTable típus az előző feladatban definiált műveletekkel (ezeket megvalósítani tehát nem kell, de használhatók)
* Törekedjünk a minimális műveletigényre, feltéve, hogy a gyakoriságtábla műveletei is optimálisan vannak megírva
* Érték: 2p

## 03_HuffmanCodes

* Írj algoritmust, ami egy láncolt formában reprezentált (tavalyi félév: Node típus) Huffman-kódfa összes lehetséges ágát bejárja és egy OutStream objektumra a kapott kódolási megfeleltetéseket kiírja
* Minden ág kerüljön új sorba
* Minden sorba kerüljön az adott karakter Huffman-kódolásbeli kódja, majd egy szóközt követve maga az érintett karakter
* Minden esetben a bal ághoz rendeljük a 0-s, a jobbhoz az 1-es kódrészletet

## 04_TernaryHuffman

* Számoljuk ki a ternáris (r = 3) Huffman-kódot erre: AZABBRAKADABRAA

## 05_EfficientCompressionAlgorithm

* Adj meg úgy három stringet, hogy az egyik a Naiv, a másik a Huffman-, a harmadik az LZW-kódolásra adja a legjobb tömörítési arányt
* Alátámasztásul játszd végig mindhárom stringre mindhárom algoritmust
* A hatékonyságvizsgálatnál most ne számoljunk a Huffman esetén a fával, a másik kettő esetén az ábécével és azok méreteivel. Csak a kódolt outputot számoljuk, a lehető legkevesebb biten, amin megadható

## 06_LzwWithCompressedDictionary

* Írj algoritmust, ami egy InStream formában megadott kódolandó stringet LZW módszerrel kódol, úgy, hogy az input mellett egy az ábécére inicializált stringtábla-referenciát is megkap, amibe az algoritmus futása végére a kódolás során előálló stringtábla tartalmát várjuk generálni
* A stringtábla értékeinél vagy definiáljunk egy adatszerkezetet, ami egy index és egy karakter párja; vagy vehetjük úgy, hogy egy string, aminek az utolsó karaktere a postfix karakter, az első "hossza-1" db pedig egy korábbi index a táblában
    * Részstringet vehetünk (igy: s = "abac", s<sub>2..3</sub> = "ba")
    * Van automatikus string-int konverziónk

# 02 - AVL-fák

## 07_IsAvl

* Írj algoritmust, ami a paraméterként megadott láncolt reprezentációjú bináris fáról eldönti, hogy AVL-fa-e
* Tegyük fel, hogy a Node típus most nem tartalmazza a "b" adattagokat, ezeket tehát nem kell és nem is lehet használni
* Szülő pointer sincs

## 08_ConvertToAvl

* Írj algoritmust, ami a paraméterként megadott aritmetikai reprezentációjú (lásd előző félév) keresőfából láncolt reprezentációjú AVL-fát épít
* A tömb csak a kulcsokat tartalmazza, a felépített fa a tanult módon legyen ábrázolva, azaz left, right, key, b adattagokkal, értelemszerűen helyes értékekkel
* Az algoritmus legyen minél hatékonyabb
* Ha menet közben kiderül, hogy a fa nem AVL-fa (a keresőfa-tulajdonságot feltételezzük, ezt nem kell ellenőrizni), NULL-lal térjünk vissza (és ne is hagyjunk lógva egy általunk foglalt memóriaterületet sem)

## 09_CountBalanced

* Írj algoritmust, ami a hagyományos láncolt ábrázolással paraméterként megadott AVL-fáról megállapítja, hogy hány 0-s egyensúlyú csúcsa van
* Adj egy minél pontosabb alsó és felső becslést is a csúcsszám függvényében a kapott számra
* A pont akkor jár, ha az algoritmus helyes és hatékony, a becslés nem teljesen triviális, és meg van indokolva

## 10_GoodSeries

* Rögzített n>0-hoz megadható-e úgy egy n méretű AVL-fa és egy felső korlát egy kulcssorozat hosszára, hogy ezen sorozat minden elemét rendre a fába szúrva, annak magassága ne változzon?
* Miért igen/miért nem?
* Ha igen, adjunk egy egy ilyen korlátot n függvényében (tetszőleges n>0-ra)
* TFH, a kulcssorozat minden eleme egyedi, és egy eleme se szerepel a kiinduló fában

# 03 - Általános fák

## 11_TwoChildren

* Írjunk algoritmust, ami megmondja "bináris láncolt" ábrázolású általános fában, hány darab kétgyerekes csúcs van!

## 12_PrintGenericTree

* Írjunk algoritmust, ami a megadott "bináris láncolt" ábrázolású általános fát kiírja a konzolra abban a formátumban, amilyenből órán parse-oltuk

## 13_ParseGenericTree

* Írjunk algoritmust, ami az órán tanult szöveges ábrázolásból a szintén órán tanult alternatív reprezentációval képes felparse-olni egy általános fát!

## 14_MaxAverage

* Írjunk algoritmust, ami megadja az alternatív ábrázolású általános fáról, melyik csúcsának a legnagyobb a leszármazottai (tehát nemcsak a közvetlen gyerekei) átlagértéke. Ha nincs ilyen, térjünk vissza NULL-lal

# 04 - B+-fák

## 15_BPlusExample

* Építsünk 5-ödfokú B+-fát az alábbi kulcsokból a megadott sorrendben:
* <10, 15, 30, 5, 12, 18, 35, 13, 14, 17, 40, 45, 96, 69, 38>
* Ezek után töröljük előbb a 18-at, majd a 30-at, végül a 35-öt
* Minden olyan lépésben, ami csúcsvágással, csúcsok közti mozgással, csúcsok összeolvadásával jár, rajzoljuk le újra a fát

# 05 - Gráf alapok

## 16_KifokBefok

* Írj struktogramot, aminek a paramétere egy irányított, élsúlyozott gráf mátrixos ábrázolása, mellékhatásként pedig két szintén paraméterként átadott tömbbe mondja meg az egyes csúcsok ki- és befokait

## 17_GrafKonverzio

* Írjunk struktogramot, aminek paramétere egy súlyozott gráf mátrixos ábrázolással, a feleadata pedig, hogy a szintén paraméterként átadott tömbbe állítsa elő ugyanennek a gráfnak az éllistás megfelelőjét
* Irányított és irányítatlan esetre is készítsük el
* Törekedjünk a leghatékonyabb megoldásra

## 18_AbszolutNyelo

* Írjunk struktogramot, ami a paraméterként átadott mátrixos ábrázolású irányított, élsúlyozatlan gráfról eldönti, van-e benne "abszolút nyelő" csúcs
* Abszolút nyelő: olyan csúcs, aminek a befoka n-1, a kifoka 0
* Az elvárt műveletigény Θ(n)

## 19_Negyzet

* Írjunk strukogramot, ami ki tudja számolni egy irányított gráf négyzetét (lásd gyakorlati anyag)
* A paraméter két éllista legyen, az egyik az input, a másik pedig a kezdetben inicializálatlan eredmény

# 06 - BFS

## 20_Utkiiras

* A feladat ugyanaz, mint a BFS-anyagban vett útkiíró feladat esetén
* Ezúttal ne rekurziót, hanem ciklust használjunk
* Törekedjünk a minimális műveletigényre

## 21_Fa

* Írjunk algoritmust, ami egy mátrixszal megadott irányítatlan gráfról el tudja dönteni, hogy a gráf fa-e
* Az algoritmust a BFS alapjaira építsük

## 22_Szinezes

* Irányítatlan gráf összefüggő részgráfjait (komponenseit) színezzük ki 1-1 színnel (azaz rendeljünk hozzá egy-egy számot)
* A reprezentáció mindegy
* Érdemes szélességi bejárásra építeni az algoritmust

# 07 - Második zh-ra

## 23_DijkstraAdj

* Írd meg a Dijkstra-algoritmust éllistás gráfábrázolással és kupacos MinQ-val (a tavalyi kupacműveletek használhatók, hívhatók, azokat külön implemetálni nem kell)
* Az algoritmus vizsgálja a Dijkstra futtathatósági feltételét, és jelezzen hibát, ha az sérül

## 24_DijkstraReverse

* Írj algoritmust, aminek az inputja egy számokkal indexelt, n+1 soros, n oszlopos mátrix, aminek értékei számok, ill. a végtelen jel, s térjen vissza egy n elemű tömbbel
* Ez a mátrix nem más, mint egy Dijkstra-futtatás d értékeinek változása, inicializásostul
* A visszatérési típusban megadott n elemű tömb legyen egy éllistás gráfábrázolás
* A legkisebb lehetséges gráfot rekonstruáljuk, aminek a paraméterben megadott mátrix lehetett a d-je
