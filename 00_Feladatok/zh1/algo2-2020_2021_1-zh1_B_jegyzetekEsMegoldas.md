# Jegyzeteim az első zh B csoportjának megírása közben + a kihagyott feladatok megoldásának leírása

## Videóból

* A videó [itt](https://youtu.be/AFrhp-8jSmc) található

### 1. - Tömörítés (hibátlan lett)*

* **: itt volt egy hiba a videós változatban, az ED felvétele kimaradt. Ez a végeredményt épp nem befolyásolta de tény, hogy rossz lett. Az itteni szótár a jó*

```
D       1
E       2
N       3
T       4
TE      5
ET      6
TET     7
TETE    8
ED      9
DE      10
ETE     11
ETT     12

4 2 5  7   2 1 6  6  7
T E TE TET E D ET ET TET

TETETETEDETETTET

N E T ET TE TET ET E D ETE T TET
3 2 4 6  7  9   6  2 1 11  4 9

D       1
E       2
N       3
T       4
NE      5
ET      6
TE      7
ETT     8
TET     9
TETE    10
ETE     11
ED      12
DE      13
ETET    14
TT      15
```

### 2. - Gráfos algoritmus (nincs értékelve)

```
removeToS(A/1 : Edge*[n], s : [1..n])
  FOR i := 1 to n
    p := A[i]
    HA p != NULL
      HA p->key == s
        t := p
        A[i] := p->next
        delete t
      KÜLÖNBEN
        AMÍG p->next != NULL és p->next->key < s
          p := p->next
        HA p->next != NULL és p->next = s
          t := p->next
          p->next := p->next->next
          delete t
```

```
A[i] = NULL
A[i] -> 5
A[i] -> 5 -> 6
A[i] -> 2 -> 4 -> 5 -> 6
A[i] -> 7
```

## Csak írásban

### 1. - BFS

* Az első ágon azt nézzük meg, ha v-be eddig el tudtunk jutni d[v] költséggel, vajon u-n keresztül olcsóbb-e, u költsége +1 költségen
* k valószínűleg a "kiindulás" rövidítése, azt mondja, hogy okés, akkor v értékét módosítottuk, folytassuk v-ből
* Mivel irányítatlan a gráf, az (u,v) él lehet hogy u értékét csökkenti, ezért tehát:

```
A: d[u]:=d[v]+1
B: k:=u
```

* Ha nem történik semmi, akkor nem történik semmi, ezért:

```
C: return
```

* A SKIP se lenne hülyeség, de bizony D-be akármit is rakunk, megindítja a Q-t, ami felesleges. Innen amúgy logikus, hogy miért Ordó itt a műveletigény - van olyan eset, amikor konstans
* A D-nél az lehet meglepő, hogy nem kell színezni, de nyilván a kiinduló csúcsot rakjuk a sorba:

```
D: Q.add(k)
```
E-nél megindítjuk a bejárást, ezek közül szintaktikailag nyilván csak egy lehet helyes:

```
E: x:=Q.rem()
```

* F esetén látjuk, miből választhatunk, az éllista bejárás inicializálása ez. x-ünk van, nem i-nk, és nem fejelemes a lista, ezért:

```
F: p:=A[x]
```

* Nem az éllisták, hanem egy éllista végét ellenőrizzük most

```
G: p != NULL
```

* x az úgymond forrácsúcs, ami javítja y-t, ami a célcsúcs, és mivel javítva van, ezzel megyünk tovább. Ezért:

```
H: d[y]:=d[x]+1
I: Q.add(y)
```

* Végül az adott éllista következő elemére megyünk, ami szintaktikailag és szemantikailag így helyes:

```
J: p:=p->next
```

### 2. - B+-fa

* "Rajzoljuk" le!

```
[ ( 4 5 ) 6 ( 6 8 9 ) 10 ( 11 12 ) 13 ( 14 15 ) ]

     6/10/13
   /  |   \   \
  /   |    \   \
4/5 6/8/9 11/12 14/15
```

* A kérdésben meg van adva sematikusan a fa, a gyökérre és a második gyerekre kíváncsiak

```
A: 6 10 13
C: 6 8 9
```

* Szúrjuk be a 7-et
* 6-nál nagyobb-egyenlő, 10-nél kisebb, ezért a 2. gyerekbe megy
* Olyan nincs, hogy "átadunk" testvérnek, csak elvenni tudunk törlésnél, ezért szakad a csúcs
* 6/7 és 8/9, ezek közül a jobb legkisebbike felmásolódik
* Emiatt a gyökér 6/8/10/13, de ez túl sok, felezzük
* A két új csúcs úgymond a 6/8 és a 10/13, ebből a jobbik kisebbik eleme felhelyeződik:

```
         10
       /    \
    6/8       13
   /  | \     |  \
  /   |  \    |   \
4/5 6/7 8/9 11/12 14/15
```

* Innen:

```
Gyökér: 10
Első szint (a gyökér a 0.): ( 6 8 ) ( 13 ) //most nem rakott amúgy szóközt a végére...
Levelek: ( 4 5 ) ( 6 7 ) ( 8 9 ) ( 11 12 ) ( 14 15 )
```

* Most az eredeti fából kell a 12-est törölni
* Megkeresem, törlöm, mivel van elég sok elemes testvér, ezért elveszek tőle egyet:

```
     6/9/13
   /  |   \   \
  /   |    \   \
4/5 6/8   9/11 14/15
```

* A sematikus ábra segít, hogy kb. milyen alakút várunk
* Innen:

```
A: 6 9 13
D: 9 11
E: 14 15
```

* Igaz-hamis:
    * Igaz-e, hogy beszúráskor esetleg valamelyik belső csúcsba be kell szúrni egy új hasítókulcsot?
        * Igaz, ha szétválasztunk egy levelet, rögtön kész a baj
    * Tekintsünk egy 6-odfokú B+ fát. Igaz-e, hogy egy gyökértől különböző belső csúcsnak lehet 2 gyermeke?
        * Hasítókulcsa épp lehet annyi, gyerekből eggyel több kell, ami itt a 3, tehát hamis

### 3. - BFS

* Inicializálás:

```
   1 2 3 4 5 6 7 8 9
d  v v v 0 v v v v v
pi n n n n n n n n n

q = <4>
```

* Megadom sorban a meneteket, a végén ott lesz a q, az első öt kérdésre tehát rendre a q értékek a válaszok:

```
1. menet (4-es feldolgozása):

   1 2 3 4 5 6 7 8 9
d  v v 1 0 v 1 v v v
pi n n 4 n n 4 n n n

q = <3;6>

2. menet (3-as feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 v 1 v v v
pi 3 3 4 n n 4 n n n

q = <6;1;2>

3. menet (6-os feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 2 1 2 v v
pi 3 3 4 n 6 4 6 n n

q = <1;2;5;7>

4. menet (1-es feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 2 1 2 v v
pi 3 3 4 n 6 4 6 n n

q = <2;5;7>

5. menet (2-es feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 2 1 2 3 v
pi 3 3 4 n 6 4 6 2 n

q = <5;7;8>

6. menet (5-ös feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 2 1 2 3 v
pi 3 3 4 n 6 4 6 2 n

q = <7;8>

7. menet (7-es feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 2 1 2 3 3
pi 3 3 4 n 6 4 6 2 7

q = <8;9>

8. menet (8-as feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 2 1 2 3 3
pi 3 3 4 n 6 4 6 2 7

q = <9>


9. menet (9-es feldolgozása):

   1 2 3 4 5 6 7 8 9
d  2 2 1 0 2 1 2 3 3
pi 3 3 4 n 6 4 6 2 7

q = <>
```

* A legnagyobb d érték 3
* d és pi:
    * 2: 2;3
    * 5: 2;6
    * 8: 3;2
    * 9: 3;7

* (5,9) él felvételével más eredményt kapnánk, ugyanis akkor a 9-es már az ötös által fel lenne fedezve (az lenne a pi-je, bár ugyanúgy 3 a d-je), mert bár a távolság azonos, de az ötös hamarabb lett feldolgozva, mint a 9-es mostani szülője, a 7-es
* Lerajzolnám én, de nem megy az olyan könnyen faluhelyen
* Viszont az s-ből 5-be menő utat könnyű rekonstruálni visszafelé 5 pi-jéből a pi-ken keresztül egészen s-ig, amit a "0"-ról (NULL) ismerünk meg:
    * 2;6;8;10;5
* 3 távolságú csúcsok:
    * A start szomszédai azok, akiknek ez a szülője: 4 és 6
    * Ezeknek a szomszédai, akiknek ezek a szülőik: 7 és 8 - ez már 2 távolság
    * A megoldás ezeknek a gyerekei: 1;3;10
* A legnagyobb távolsághoz folytatom az előző gondolatmenetet, mert nincs túl sok csúcs (nyilván egyszerűbb lett volna tényleg lerajzolni):
    * 1, 3 és 10 gyerekei vannak 4 távolságra: 5
    * 5 gyerekei 5 távolságra: 9
    * 9-nek nincs gyereke, tehát a megoldás 5, ez a leghosszabb távolság

### 4. - AVL-fa

* 10,9,5,3,1
* Gyanúsan hasonlít az A csoportéra

```
10
```

```
  10-
 /
9
```

```
    10--
   /
  9-
 /
5
```

* (--,-)

```
  9=
 / \
5  10
```

```
    9-
   / \
  5- 10
 /
3
```

```
      9-
     / \
    5--10
   /
  3-
 /
1
```

* (--,-), régi cimbora

```
      9-
     / \
    3= 10
   / \
  1   5
```

* Tehát 2 forgatás volt, ez volt a kérdés
* Gyökér egyensúlya: -
* Hány elem törölhető ki? Itt már tudjuk, hogy arra gondol aköltő, hogy egymás után. Ha a 10-es törlöm ki, (--,=) forgatás lesz, ami valid, de majd a következő már csökkenteni fogja a magasságot. Ha bármi mást, akkor az egyik levél kiesik a szimmetrikus bal gyerekből, s emiatt a legközelebbi esetben forgatni kell: ezért 1
* A rendezettség szükségessége miatt: nem

```
( ( (10) 12= (15) ) 27+ ( ( (34) 38= (40) ) 42- ( 47 ) ) )

        27+
      /    \
   12=      42-
  / \      /  \
10  15   38=  47
        / \
       34  40
```

* Szúrjuk be a 41-et:

```
        27+
      /    \
   12=      42--
  / \      /  \
10  15   38+  47
        / \
       34  40+
            \
             41
```

* (--,+)

```
       27+
      /   \
   12=     40=
  / \     /  \
10  15   38-  42=
        /    /  \
       34  41   47
```

* 40-es csúcs címkéje: =
* Gyökér egyensúlya: +
* Famagasság: 3
* Szúrjuk be a 48-at:

```
       27++
      /   \
   12=     40+
  / \     /  \
10  15   38-  42+
        /    /  \
       34  41   47+
                  \
                   48
```

* (++,+)

```
        40=
       /   \
    27=     42+
   /  \     /  \
  12=  38- 41  47+
 / \   /         \
10 15 34         48+
```

* Gyökér: 40
* 2. szint: 12=,38-,41=,47+ - fontos az = a levélnél is
* nem, mert volt forgatás (és nem =-s, ami nem is lehet beszúrásnál persze)
